# Custom Nornir invetory plugins

from nornir.core.deserializer.inventory import Inventory
from nornir.core.inventory import ConnectionOptions
from xl2dict import XlToDict
from pprint import pprint


class ExcelInventory(Inventory):
    """ExcelInventory plugin is an inventory created from an excel file 
    named "device_inventory.xlsx" containing list of hostnames/IPs,
    their driver type, groups they are member of and attributes

    Excel file format is in file "device_inventory_file_format.txt"
    """

    def __init__(self, **kwargs):
        """Build inventory from excel file"""

        # convert excel file and save to dict
        hosts_dict = self.excel_to_dict(excel_file_path="device_inventory.xlsx",
                                        sheet_name="Hosts")
        group_dict = self.excel_to_dict(excel_file_path="device_inventory.xlsx",
                                        sheet_name="Groups")
        defaults_dict = self.excel_to_dict(excel_file_path="device_inventory.xlsx",
                                           sheet_name="Defaults")

        # Format of hosts dict to build inventory hosts
        """
        hosts = {
            "host1": {
                "hostname": "hostnamex",
                "platform": "platformx",
                "port": "portx",
                "data": {"data1": "value1", "data2": "value2", 
                         "data3": "value3"},
                "groups": ["my_group1"],
            },
            "host2": {
                "hostname": "hostnamex",
                "platform": "platformx",
                "port": "portx",
                "data": {"data1": "value1", "data2": "value2", 
                         "data3": "value3"},
                "groups": ["my_group1"],
            },
        }
        """

        # Building hosts dict to pass and transform to the inventory
        hosts = {}
        for device in hosts_dict:
            host = {}
            data = {}
            for column, value in device.items():
                # 1st "name" = "corpt-aswt-01"
                # 2nd "hostname" = "corpt-aswt-01"/"10.10.10.1"
                # 3rd "platform" = "ios"
                # 4th "port" = "22"
                # 5th "group membership"
                # XlToDict saves numbers as float e.g.(22.0),
                # change float to int (22) and to str ("22")
                if type(value) != str:
                    value = int(value)
                    value = str(value)
                if (value != "" and column != "group membership"):
                    if (column == "hostname" or column == "platform" or 
                            column == "port"):
                        host[column] = value.lower()
                    else:
                        # check number of values in the excel cell
                        value = value.lower()
                        data_values = value.split(", ")
                        if len(data_values) == 1:  # if one value: assign it
                            # 6th location = "london"
                            data[column] = data_values[0]
                        elif len(data_values) == 2 and data_values[1] == "":
                            # 7th role = "access, "
                            # if one value but its a part of a list: assign
                            # the list but delete an empty/last entry
                            del data_values[1]
                            data[column] = data_values
                        else:
                            # 8th vlan = "100, 200"
                            data[column] = data_values
            host["data"] = data  # assign all custom attibutes

            # assigning groups the host is member of
            group_list = device["group membership"].lower()
            group_list = group_list.split(", ")
            if group_list[0] != "":
                # verify if the groups the host is member of are defined 
                # in the "Groups" tab
                for host_group in group_list:
                    group_count = 0
                    for group in group_dict:
                        if group["group name"].lower() == host_group:
                            group_count = group_count +1
                    if group_count == 0:
                        raise (Exception(f"Group \"{host_group}\" the host \"{host['hostname']}\" is member of is not defined in the \"Groups\" tab in excel inventory file"))
                    elif group_count > 1:
                        raise (Exception(f"Group \"{host_group}\" is defined multiple times in the \"Groups\" tab in excel inventory file"))

                host["groups"] = group_list

            # add host to hosts dict
            hosts[device["name"]] = host

        # Format of groups dict to build inventory groups
        """
        groups = {
            "my_group1": {
                "data": {
                    "more_data1": "more_value1",
                    "more_data2": "more_value2",
                    "more_data3": "more_value3",
                }
            }
        }
        """

        # Building groups dict to pass and transform to the inventory
        groups = {}
        # check if any group is defined
        if group_dict:
            for group in group_dict:
                g = {}
                data = {}
                for column, value in group.items():
                    # XlToDict saves numbers as float e.g.(22.0),
                    # change float to int (22) and to str ("22")
                    if type(value) != str:
                        value = int(value)
                        value = str(value)
                    if (value != "" and column != "group membership"):
                        if (column == "group name" or column == "platform" or
                                column == "port"):
                            g[column] = value.lower()
                        else:
                            # check number of values in the excel cell
                            value = value.lower()
                            data_values = value.split(", ")
                            if len(data_values) == 1:  # if one value:assign it
                                data[column] = data_values[0]
                            elif (len(data_values) == 2 and
                                    data_values[1] == ""):
                                # if one value but its a part of a list:
                                # assign the list but delete an empty entry
                                del data_values[1]
                                data[column] = data_values
                            else:
                                data[column] = data_values

                g["data"] = data  # assign all custom attibutes

                # assigning groups the group is member of
                group_list = group["group membership"].lower()
                group_list = group_list.split(", ")
                if group_list[0] != "":
                    g["groups"] = group_list

                # add group to groups dict
                groups[group["group name"].lower()] = g

        # Format of defaults dict to build inventory default attributes
        # defaults = {"data": {"location": "internet", "language": "Python"}}

        # Building defaults dict to pass and transform to the inventory
        defaults = {}
        if defaults_dict:  # check if any default attiribute is defined
            for default_data in defaults_dict:
                data = {}
                for column, value in default_data.items():
                    # XlToDict saves numbers as float e.g.(22.0),
                    # change float to int (22) and to str ("22")
                    if type(value) != str:
                        value = int(value)
                        value = str(value)
                    if value != "":
                        if (column == "platform" or column == "port"):
                            defaults[column] = value.lower()
                        else:
                            # check number of values in the excel cell
                            value = value.lower()
                            data_values = value.split(", ")
                            if len(data_values) == 1:  # if one value:assign it
                                data[column] = data_values[0]
                            elif (len(data_values) == 2 and
                                    data_values[1] == ""):
                                # if one value but its a part of a list:
                                # assign the list but delete an empty entry
                                del data_values[1]
                                data[column] = data_values
                            else:
                                data[column] = data_values

                # add custom attributes to defaults data dict
                defaults["data"] = data

        # passing the data to the parent class so the data is
        # transformed into actual Host/Group objects
        # and set default data for all hosts
        super().__init__(hosts=hosts, groups=groups,
                         defaults=defaults, **kwargs)

    def excel_to_dict(self, excel_file_path, sheet_name):
        device_dict = XlToDict()
        try:
            return device_dict.fetch_data_by_column_by_sheet_name(
                file_path=excel_file_path, sheet_name=sheet_name)
        except FileNotFoundError:
            print("\nCan't find device inventory excel file.")
            print("Make sure your file is named \"device_inventory.xlsx\" and it is in the same folder as the script")
        exit(0)
