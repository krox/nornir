from napalm.ios.ios import IOSDriver
import textfsm
from pathlib import Path


class CustomIOSDriver(IOSDriver):
    """ Extend IOSDriver class with own functions/tasks.
    Not all class functions structurize command output data (render through 
    the TextFSM template and save to dictionary as keyword arguments), some 
    just save the output as a one string. Ideally all functions should 
    structurize the data - this is to do task.
    """

    def get_aaa_servers(self):
        command = "show aaa servers"
        # send command to the device and save the output
        output = self._send_command(command)
        # TextFSM template file
        template_file = Path("ntc_templates/ios/show_aaa_servers.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        # structurize data
        table_keys = re_table.header
        final_dict = {}
        for row in data:
            fsm_dict = dict(zip(table_keys, row))
            # some Cisco models have additionally server platform state
            # additionally to the state
            if fsm_dict["PLATFORM_STATE"] != "":
                fsm_dict["STATE"] = fsm_dict["PLATFORM_STATE"]
            del fsm_dict["PLATFORM_STATE"]
            final_dict[fsm_dict["IP_ADDRESS"]] = fsm_dict

        return final_dict

    def get_auth_sessions(self):
        command = "show authentication sessions"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/cisco_ios_show_authentication_sessions.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        table_keys = re_table.header
        final_list = []
        for row in data:
            fsm_dict = dict(zip(table_keys, row))
            final_list.append(fsm_dict)

        return final_list

    def get_auth_session_number(self):
        output = self.get_auth_sessions()
        
        MAB_session_number = 0
        dot1x_session_number = 0
        unknown_session_number = 0
        total_session_number = 0

        for session in output:
            if session["METHOD"] == "dot1x":
                dot1x_session_number += 1
            elif session["METHOD"] == "mab":
                MAB_session_number += 1
            else:
                unknown_session_number += 1

        total_session_number = unknown_session_number + MAB_session_number + dot1x_session_number
        return {"MAB_sessions": MAB_session_number, "dot1x_sessions": dot1x_session_number,
                "unknown_sessions": unknown_session_number, "all_sessions": total_session_number}

    def get_auth_sessions_dot1x_details(self):
        command = "show authentication sessions method dot1x details"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/show_auth_sessions_method_dot1x_details.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        table_keys = re_table.header
        final_dict = {}
        for row in data:
            fsm_dict = dict(zip(table_keys, row))
            final_dict[fsm_dict["MAC"]] = fsm_dict

        return final_dict

    def get_auth_sessions_mab_details(self):
        command = "show authentication sessions method mab details"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/show_auth_sessions_method_mab_details.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        table_keys = re_table.header
        final_dict = {}
        for row in data:
            fsm_dict = dict(zip(table_keys, row))
            final_dict[fsm_dict["MAC"]] = fsm_dict

        return final_dict

    def get_test_radius_user_auth(self, command, user, server_name):
        """ command = test aaa group radius server name [server_name] [user]
        [password] new-code
        """
        output = self._send_command(command)
        output = output.split("\n")

        # structurize output data
        final_dict = {}
        final_dict["username"] = user
        # IOS output doesn't clearly say if user is rejected or server is dead
        if output[0] == "User successfully authenticated":
            final_dict["authenticated"] = True
            final_dict["reason"] = output[0]
        if output[0] == "User rejected" and len(output) == 1:
            final_dict["authenticated"] = False
            final_dict["reason"] = "Incorrect user credentials"
        if output[0] == "User rejected" and len(output) == 2:
            final_dict["authenticated"] = False
            final_dict["reason"] = "Specified server is dead"
        if output[0] == "Unable to find specified server in group.":
            final_dict["authenticated"] = False
            final_dict["reason"] = "Unable to find specified server in group"

        final_dict["radius_server"] = server_name
        return final_dict

    def get_test_radius_user_auth2(self, user: str, password: str, servers: list):
        """Test RADIUS user authentication
        Server has to be in RADIUS group
        admin rights required

        IOS: test aaa group radius server name [server_name] [user] [password] new-code
        Legacy IOS: test aaa group radius server [IP_addr] [user] [password] new-code

        Arguments:
            user: username
            password: password
            servers: list of dicts containing servers
                example: ["server_name": "ISE_1", "IP_addr": "10.10.10.1"}, ...]

        server_name can be anything if device is legacy IOS
        """
        IOS_version = self.get_os_main_version()[0]["VERSION"]
        final_dict = {}
        final_dict["username"] = user
        i = 0
        for server in servers:
            if int(IOS_version) <= 15 and int(IOS_version) > 3:
                command = f"test aaa group radius server {server['IP_addr']} auth-port 1812 acct-port 1813 {user} {password} new-code "
            else:
                command = f"test aaa group radius server name {server['server_name']} {user} {password} new-code"
            output = self._send_command(command)
            output = output.split("\n")

            # structurize output data
            auth_result = {}
            # IOS output doesn't clearly say if user is rejected or server is dead
            if output[0] == "User successfully authenticated":
                auth_result["authenticated"] = True
                auth_result["reason"] = output[0]
            elif output[0] == "User rejected" and len(output) == 1:
                auth_result["authenticated"] = False
                auth_result["reason"] = "Incorrect user credentials"
            elif output[0] == "User rejected" and len(output) == 2:
                auth_result["authenticated"] = False
                auth_result["reason"] = "Specified server is dead"
            elif output[0] == "Unable to find specified server in group.":
                auth_result["authenticated"] = False
                auth_result["reason"] = "Unable to find specified server in group"
            else:
                auth_result["authenticated"] = False
                auth_result["reason"] = output[0]
            auth_result["radius_server"] = servers[i]["server_name"]
            auth_result["radius_server_IP"] = servers[i]["IP_addr"]
            final_dict[f"server_{i+1}"]  = auth_result
            i = i+1
            
        return final_dict

    def get_radius_groups(self):
        command = "show run | i aaa group server radius"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/show_run_i_aaa_group_server_radius.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)
        
        radius_group_list = []
        for radius_group in data:
            radius_group_list.append(radius_group[0])
        final_dict = {}
        final_dict["radius_group_list"] = radius_group_list
        return final_dict

    def get_ip_device_tracking_all(self):
        # Different IOS versions have different command syntax and output
        result = self.get_os_main_version()
        OS_main_version = result[0]["VERSION"]
        if int(OS_main_version) >= 16:
            command = "show device-tracking database"
            output = self._send_command(command)
            template_file = Path("ntc_templates/ios/ios_16_and_above/show_device-tracking_database.tpl")
        else:
            command = "show ip device tracking all"
            output = self._send_command(command)
            template_file = Path("ntc_templates/ios/show_ip_device_tracking_all.tpl")

        # Check if device tracking is disabled
        tmp_list = output.split("\n")
        if tmp_list[0] == "IP Device Tracking = Disabled":
            return "IP Device Tracking Disabled"
        else:
            with open(template_file, 'r') as f:
                # render the output through the template
                re_table = textfsm.TextFSM(f)
                data = re_table.ParseText(output)

            table_keys = re_table.header
            final_list = []
            for row in data:
                fsm_dict = dict(zip(table_keys, row))
                final_list.append(fsm_dict)

            return final_list

    def get_os_main_version(self):
        """ used by other function if there is a difference in syntax and output
        beteween different IOS versions
        """
        command = "show version | i Version"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/show_version i Version.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        table_keys = re_table.header
        final_list = []
        for row in data:
            fsm_dict = dict(zip(table_keys, row))
            final_list.append(fsm_dict)

        return final_list

    def get_snmp_contact(self):
        command = "show run | i snmp-server contact"
        output = self._send_command(command)
        return output

    def get_cts_server_list(self):
        """ get TrustSec server-list
        """
        command = "show cts server-list"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/show_cts_server-list.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        table_keys = re_table.header
        final_list = []
        for row in data:
            fsm_dict = dict(zip(table_keys, row))
            final_list.append(fsm_dict)

        return final_list

    def get_cts_role_based_sgt_map_all(self):
        """Get TrustSec sgt mapping
        """
        command = "show cts role-based sgt-map all"
        output = self._send_command(command)
        return output

    def get_cts_environment_data(self):
        """get TrustSec environment data
        """
        command = "show cts environment-data"
        output = self._send_command(command)
        return output

    def get_cts_pacs(self):
        """get TrustSec PAC keys
        """
        command = "show cts pacs"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/show_cts_pacs.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        table_keys = re_table.header
        final_list = []
        for row in data:
            fsm_dict = dict(zip(table_keys, row))
            final_list.append(fsm_dict)

        return final_list

    def get_cts_environment_data_state(self):
        """ get TrustSec state. When new TrustSec servers are added 
        the state and status are going through multiple phases as below:
        state: START, COMPLETE
        status: Cleared, Successfull, Failed
        This function is to get and/or validate TrustSec servers state
        and status
        """
        command = "show cts environment-data | i state|status"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/show_cts_environment_data_state.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        table_keys = re_table.header
        fsm_dict = dict(zip(table_keys, data[0]))
        return fsm_dict

    def get_flash(self):
        """To discover the flash disc name as it is different depends 
        on the device hardware model. It also gets info about the disc 
        space utilisation and list of files.
        """
        command = "dir"
        output = self._send_command(command)

        template_file = Path("ntc_templates/ios/dir.tpl")
        with open(template_file, 'r') as f:
            # render the output through the template
            re_table = textfsm.TextFSM(f)
            data = re_table.ParseText(output)

        final_dict = {}
        final_dict["flash_name"] = data[0][0]
        final_dict["total_space"] = int(data[0][2])
        final_dict["free_space"] = int(data[0][3])
        final_dict["used_space"] = int(data[0][2]) - int(data[0][3])
        file_list = []
        for row in data:
            file_list.append(row[1])
        final_dict["file_list"] = file_list
        return final_dict
