Value ID (.*)
Value PRIORITY (.*)
Value IP_ADDRESS (.*)
Value AUTH_PORT (.*)
Value ACCT_PORT (.*)
Value STATE (.+?)
Value PLATFORM_STATE (.+?)


Start
  ^RADIUS: id ${ID}, priority ${PRIORITY}, host ${IP_ADDRESS}, auth-port ${AUTH_PORT}, acct-port ${ACCT_PORT}.*$$
  ^\s+State: current ${STATE},.*$$
  ^\s+Platform State: current ${PLATFORM_STATE},.*$$
  ^\s+Quarantined:.*$$ -> Record

EOF