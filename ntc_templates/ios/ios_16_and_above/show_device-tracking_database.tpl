Value IP (\S+)
Value MAC ([0-9a-fA-F]{4}\.[0-9a-fA-F]{4}\.[0-9a-fA-F]{4})
Value VLAN ([0-9]+)
Value INTERFACE (\S+)
Value PROBE_TIMEOUT (\S+)
Value STATE (\S+)
Value SOURCE (\S+)

Start
  ^\s+Network Layer Address.*Time left.*$$ -> Catch

Catch
  ^${SOURCE}\s+${IP}\s+${MAC}\s+${INTERFACE}\s+${VLAN}\s+\d+\s+\S+\s+${STATE}.*$$ -> Record