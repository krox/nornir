Value INTERFACE (.+?)
Value MAC (.+?)
Value IPv6_ADDRESSS (.+?)
Value IPv4_ADDRESSS (.*)
Value USERNAME (.*)
Value STATUS (.+?)
Value DOMAIN (.+?)
Value SESSION (\w+?)
Value CURRENT_POLICY (.*)
Value URL_REDIRECT_ACL (.*)
Value URL_REDIRECT (.*)
Value ACS_ACL (.*)


Start
  ^\s*Interface:\s+${INTERFACE}$$ -> Continue
  ^\s+MAC Address:\s+${MAC}$$ -> Continue
  ^\s+IPv6 Address:\s+${IPv6_ADDRESSS}$$ -> Continue
  ^\s+IPv4 Address:\s+${IPv4_ADDRESSS}$$ -> Continue
  ^\s+User-Name:\s+${USERNAME}$$ -> Continue
  ^\s+Status:\s+${STATUS}$$ -> Continue
  ^\s+Domain:\s+${DOMAIN}$$ -> Continue
  ^\s+Common Session ID:\s+${SESSION}$$ -> Continue
  ^\s+Current Policy:\s+${CURRENT_POLICY}$$ -> Continue
  ^\s+URL Redirect ACL:\s+${URL_REDIRECT_ACL}$$
  ^\s+URL Redirect:\s+${URL_REDIRECT}$$
  ^\s+ACS ACL:\s+${ACS_ACL}$$
  ^----+ -> Continue.Record