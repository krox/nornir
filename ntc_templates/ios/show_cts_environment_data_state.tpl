Value current_state (\S*)
Value last_state (\S*)

Start
  ^Current state\s+=\s+${current_state}.*$$ 
  ^Last status\s+=\s+${last_state}.*$$ -> Record

EOF