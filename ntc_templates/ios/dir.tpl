Value Filldown flash_name (.*)
Value file_name (.*)
Value Fillup total_space (.*)
Value Fillup free_space (.*)


Start
  ^Directory of ${flash_name}.*$$ -> files

files
  ^.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+.*\s+${file_name}.*$$ -> Record
  ^${total_space}\s+.*\s+.*\s+\(${free_space}\s+bytes.*$$

EOF