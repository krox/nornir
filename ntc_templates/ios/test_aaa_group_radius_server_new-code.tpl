Value RESULT (.*)

Start
  ^User successfully ${RESULT}.*$$ -> Record
  ^User ${RESULT}.*$$ -> Record
  ^Unable to find specified server in group.*$$ -> Record

EOF