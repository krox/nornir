Value IP_addr (.*)
Value port (.*)
Value state (.*)

Start
  ^\s+\*Server:\s+${IP_addr},\s+port\s+${port},.*$$
  ^\s+Status\s+=\s+${state}.*$$ -> Record

EOF