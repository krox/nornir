Nornir
=======

An automation tool to run tasks on network devices.

It is a set of various tasks that are grouped into few categories:

* getters to collect information
* configuration tasks
* validation task to check if the device config is compliant with the provided data
* other tasks like test authentication, clear session, delete files...

Some of the tasks like RADIUS migration consist of multiple small tasks to gather information, validate the configuration before/after the change and rollback if any issue has been encountered; for example, added server state is down.

Nornir is an automation framework to allow building an inventory of hosts and groups, running tasks in parallel on selected hosts and provide logging capability.
Own custom inventory plugin was built to allow building an inventory of hosts and groups from an excel file.

The tool uses NAPALM in the background for connecting to devices. It provides a common layer for various vendors and can be easily extended to support more devices. Device configuration leverages a candidate, merge, rollback configs and commit, rollback functions to allow for automatic rollback if any issue is encountered, atomic changes (all command lines succeeded or none are applied) and implement only lines that are not yet in the device config.
Config tasks can be also run in dry/test mode to test the change without making changes to the device configuration.

On top of NAPALM built-in getters, own getters and config tasks were added.
Configuration tasks are rendered through ninja2 templates to make the solution scalable and easy to manage.

The tool uses NTC templates (TextFSM) if collected data is in text format and needs to be structured.

tqdm task progress bars were added to present progress of the running tasks in real-time.


Requirements
=======
```
Python 3.6.2 or newer
```


How to run
=======
```
$ git clone https://gitlab.com/krox/nornir.git
$ pip install -r requirements.txt
$ Nornir_menu.py
```

## List of available tasks (up to date)
### Getters
```
1. get_aaa_servers
2. get_arp_table
3. get_auth_session_number
4. get_auth_sessions
5. get_auth_sessions_dot1x_details
6. get_auth_sessions_mab_details
7. get_bgp_config
8. get_bgp_neighbors
9. get_bgp_neighbors_detail
10. get_config
11. get_environment
12. get_facts
13. get_flash
14. get_interfaces
15. get_interfaces_counters
17. get_interfaces_ip
18. get_ip_device_tracking_table
19. get_ipv6_neighbors_table
20. get_lldp_neighbors
21. get_lldp_neighbors_detail
22. get_mac_address_table
23. get_network_instances
24. get_ntp_peers
25. get_ntp_servers
26. get_ntp_stats
27. get_optics
28. get_probes_config
29. get_probes_results
30. get_radius_groups
31. get_snmp_information
32. get_trustsec_PAC_keys
33. get_trustsec_env
34. get_trustsec_servers
35. get_trustsec_sgt_map
36. get_users
```
### Configuration tasks
```
1. add_radius_dynamic_auth
2. add_radius_server
3. add_radius_server_to_group
4. bounce_interface
5. disable_TrustSec_enforcement
6. enable_TrustSec_enforcement
7. migrate_radius_servers
8. migrate_TrustSec_servers
9. remove_radius_dynamic_auth
10. remove_radius_server
11. remove_radius_server_from_group
12. replace_config
```
### Other tasks
```
1. clear_auth_sessions
2. delete_files
3. test_radius_user_auth
```
### Validate
Provide data to validate device against

Example
=======
![alt text](migrate_radius_servers.jpg "example_task_image")