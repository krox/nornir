from nornir.plugins.tasks import networking
from nornir.plugins.tasks import text
from nornir.plugins.tasks import data
from nornir.core.exceptions import NornirSubTaskError
import logging
import pdb
import tempfile
from napalm.base.utils import py23_compat
import os
import uuid
from datetime import datetime
import re
import time
from tqdm import tqdm
from colorama import Fore


""" This module contains tasks to be run by Nornir.
Tasks are separated on get tasks to collect information, validate task to 
check if the provided data is compliant, template tasks to build configuration, 
configuration tasks (add/remove) to apply config on hosts or command executing
tasks(test, clear, delete etc.).
Each task should have relevant tqdm progress bars added to display progress
of the running subtasks and write basic information while the task is running.
It also should have logging level assigned to allow running the task with 
desired logging level.
Some tasks can be really basic consisting just of one subtask or can be 
complex consisting of many get, change, validation and rollback subtasks (see 
migrate_radius_servers task)
"""

def get_facts(task, task_progress_bar: tqdm, task_logging_level=logging.INFO):
    """Get device general info
    """
    tqdm.write(f"{task.host}: getting facts...")#write to task progress console
    # task.host = hostname of a host that the task is running for

    # run the task
    task.run(task=networking.napalm_get,
                    name="Get device info",
                    getters=["facts"],
                    severity_level=task_logging_level)
    # update task progress bar once the task completed
    task_progress_bar.update()

def get_aaa_servers(task, task_progress_bar: tqdm,
                    task_logging_level=logging.INFO):
    """Get AAA servers configured on host
    """
    tqdm.write(f"{task.host}: getting aaa servers...")

    r = task.run(task=networking.napalm_get,
                    name="Get AAA servers",
                    getters=["aaa_servers"],
                    severity_level=task_logging_level)
    task.host["aaa_servers"] = r.result["aaa_servers"]
    
    task_progress_bar.update()

def get_arp_table(task, task_progress_bar: tqdm,
                  task_logging_level=logging.INFO):
    """Get ARP table
    """
    tqdm.write(f"{task.host}: getting ARP table...")

    task.run(task=networking.napalm_get, name="Get ARP table",
             getters=["get_arp_table"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_auth_sessions(task, task_progress_bar: tqdm, 
                      tqdm_message="getting authentication sessions...",
                      task_logging_level=logging.INFO):
    """Get authentication sessions (802.1x/MAB)
    """
    tqdm.write(f"{task.host}: {tqdm_message}")
    
    task.run(task=networking.napalm_get,
             name="Get authentication sessions",
             getters=["get_auth_sessions"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_auth_session_number(task, task_progress_bar: tqdm, 
                            tqdm_message="getting authentication sessions...",
                            task_logging_level=logging.INFO):
    """Get authentication session number (802.1x/MAB)
    """
    tqdm.write(f"{task.host}: {tqdm_message}")
    
    task.run(task=networking.napalm_get,
             name="Get authentication session number",
             getters=["get_auth_session_number"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_auth_sessions_dot1x_details(task, task_progress_bar: tqdm, 
                      tqdm_message="getting dot1x authentication sessions details...",
                      task_logging_level=logging.INFO):
    """Get dot1x authentication sessions details
    """
    tqdm.write(f"{task.host}: {tqdm_message}")
    
    task.run(task=networking.napalm_get,
             name="Get dot1x authentication sessions details",
             getters=["get_auth_sessions_dot1x_details"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_auth_sessions_mab_details(task, task_progress_bar: tqdm, 
                      tqdm_message="getting mab authentication sessions details...",
                      task_logging_level=logging.INFO):
    """Get mab authentication sessions details
    """
    tqdm.write(f"{task.host}: {tqdm_message}")
    
    task.run(task=networking.napalm_get,
             name="Get mab authentication sessions details",
             getters=["get_auth_sessions_mab_details"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_bgp_config(task, task_progress_bar: tqdm,
                  task_logging_level=logging.INFO):
    """Get BGP config
    """
    tqdm.write(f"{task.host}: getting BGP config...")

    task.run(task=networking.napalm_get, name="Get BGP config",
             getters=["get_bgp_config"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_bgp_neighbors(task, task_progress_bar: tqdm,
                  task_logging_level=logging.INFO):
    """Get BGP neighbors
    """
    tqdm.write(f"{task.host}: getting BGP neighbors...")

    task.run(task=networking.napalm_get, name="Get BGP neighbors",
             getters=["get_bgp_neighbors"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_bgp_neighbors_detail(task, task_progress_bar: tqdm,
                             task_logging_level=logging.INFO):
    """Get BGP neighbors details
    """
    tqdm.write(f"{task.host}: getting BGP neighbors detail...")

    task.run(task=networking.napalm_get, name="Get BGP neighbors detail",
             getters=["get_bgp_neighbors_detail"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_config(task, task_progress_bar: tqdm, dest_config_name="config",
               tqdm_message="getting configuration...",
               task_logging_level=logging.INFO):
    """Get startup, running and candidate (if device supports it) configs
    as a list of dict
    """
    tqdm.write(f"{task.host}: {tqdm_message}")
    
    r = task.run(task=networking.napalm_get,
                 name="Get device config",
                 getters=["config"],
                 severity_level=task_logging_level)
    task.host[dest_config_name] = r.result["config"]

    task_progress_bar.update()

def get_environment(task, task_progress_bar: tqdm,
                    task_logging_level=logging.INFO):
    """Get environment data
    """
    tqdm.write(f"{task.host}: getting environment data...")

    task.run(task=networking.napalm_get, name="Get environment",
             getters=["get_environment"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_flash(task, task_progress_bar: tqdm, 
              tqdm_message="getting flash disc data and file list...",
              task_logging_level=logging.INFO):
    """Get flash disc parameters and list of files
    """
    tqdm.write(f"{task.host}: {tqdm_message}")
    
    task.run(task=networking.napalm_get,
             name="Get flash disc",
             getters=["get_flash"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_interfaces(task, task_progress_bar: tqdm,
                   task_logging_level=logging.INFO):
    """Get interfaces
    """
    tqdm.write(f"{task.host}: getting interfaces...")

    task.run(task=networking.napalm_get, name="Get interfaces",
             getters=["get_interfaces"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_interfaces_counters(task, task_progress_bar: tqdm,
                           task_logging_level=logging.INFO):
    """Get interface counters
    """
    tqdm.write(f"{task.host}: getting interfaces counters...")

    task.run(task=networking.napalm_get, name="Get interfaces counters",
             getters=["get_interfaces_counters"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_interfaces_ip(task, task_progress_bar: tqdm, 
                      task_logging_level=logging.INFO):
    """Get interfaces IP addresses
    """
    tqdm.write(f"{task.host}: getting interfaces IP addresses...")

    task.run(task=networking.napalm_get, name="Get interfaces IP",
             getters=["interfaces_ip"],  severity_level=task_logging_level)
    
    task_progress_bar.update()
        
def get_ip_device_tracking_table(task, task_progress_bar: tqdm,
                                 task_logging_level=logging.INFO):
    """Get IP device tracking table
    """
    tqdm.write(f"{task.host}: getting IP device tracking table...")

    r = task.run(task=networking.napalm_get,
                 name=f"GET IP device tracking table",
                 getters=["get_ip_device_tracking_all"],
                 severity_level=task_logging_level)

    task_progress_bar.update()

    if r[0].result["get_ip_device_tracking_all"] == "IP Device Tracking Disabled":
        task.run(task=data.echo_data, name = "IP Device Tracking is disabled",
                 severity_level=logging.INFO)
        r[0].failed = True
        tqdm.write(f"{Fore.RED}{task.host}: IP device tracking table is disabled on device")

def get_ipv6_neighbors_table(task, task_progress_bar: tqdm,
                             task_logging_level=logging.INFO):
    """Get IPv6 neighbors
    """
    tqdm.write(f"{task.host}: getting IPv6 neighbors ...")

    task.run(task=networking.napalm_get, name="Get IPv6 neighbors",
             getters=["get_ipv6_neighbors_table"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_lldp_neighbors(task, task_progress_bar: tqdm,
                       task_logging_level=logging.INFO):
    """Get LLDP neighbors
    """
    tqdm.write(f"{task.host}: getting LLDP neighbors...")

    task.run(task=networking.napalm_get, name="Get LLDP neighbors",
             getters=["get_lldp_neighbors"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_lldp_neighbors_detail(task, task_progress_bar: tqdm,
                       task_logging_level=logging.INFO):
    """Get LLDP neighbors detail
    """
    tqdm.write(f"{task.host}: getting LLDP neighbors detail...")

    task.run(task=networking.napalm_get, name="Get LLDP neighbors detail",
             getters=["get_lldp_neighbors_detail"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()


def get_mac_address_table(task, task_progress_bar: tqdm, 
                          task_logging_level=logging.INFO):
    """Get MAC address table
    """
    tqdm.write(f"{task.host}: getting MAC address table...")

    task.run(task=networking.napalm_get, name="Get MAC address table",
             getters=["get_mac_address_table"],
             severity_level=task_logging_level)

    task_progress_bar.update()

def get_network_instances(task, task_progress_bar: tqdm,
                          task_logging_level=logging.INFO):
    """Get network instances
    """
    tqdm.write(f"{task.host}: getting network instances ...")

    task.run(task=networking.napalm_get, name="Get network instances",
             getters=["get_network_instances"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_ntp_peers(task, task_progress_bar: tqdm,
                  task_logging_level=logging.INFO):
    """Get NTP peers
    """
    tqdm.write(f"{task.host}: getting NTP peers...")

    task.run(task=networking.napalm_get, name="Get NTP peers",
             getters=["get_ntp_peers"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_ntp_servers(task, task_progress_bar: tqdm,
                  task_logging_level=logging.INFO):
    """Get NTP servers 
    """
    tqdm.write(f"{task.host}: getting NTP servers...")

    task.run(task=networking.napalm_get, name="Get NTP servers ",
             getters=["get_ntp_servers"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_ntp_stats(task, task_progress_bar: tqdm,
                  task_logging_level=logging.INFO):
    """Get NTP stats
    """
    tqdm.write(f"{task.host}: getting NTP stats...")

    task.run(task=networking.napalm_get, name="Get NTP stats",
             getters=["get_ntp_stats"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_optics(task, task_progress_bar: tqdm,
               task_logging_level=logging.INFO):
    """Get optics
    """
    tqdm.write(f"{task.host}: getting optics...")

    task.run(task=networking.napalm_get, name="Get optics",
             getters=["get_optics"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_probes_config(task, task_progress_bar: tqdm,
                      task_logging_level=logging.INFO):
    """Get probes configuration
    """
    tqdm.write(f"{task.host}: getting probes config...")

    task.run(task=networking.napalm_get, name="Get probes config",
             getters=["get_probes_config"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_probes_results(task, task_progress_bar: tqdm,
                       task_logging_level=logging.INFO):
    """Get probes result
    """
    tqdm.write(f"{task.host}: getting probes results...")

    task.run(task=networking.napalm_get, name="Get probes results",
             getters=["get_probes_results"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_radius_groups(task, task_progress_bar: tqdm, 
                      task_logging_level=logging.INFO):
    """Get configured RADIUS server groups
    """

    tqdm.write(f"{task.host}: getting RADIUS groups...")

    r = task.run(task=networking.napalm_get, getters=["get_radius_groups"],
                 severity_level=task_logging_level)
    task.host["radius_groups"] = r[0].result["get_radius_groups"]
    
    task_progress_bar.update()


def get_snmp_information(task, task_progress_bar: tqdm,
                         task_logging_level=logging.INFO):
    """Get SNMP information
    """
    tqdm.write(f"{task.host}: getting SNMP information...")

    task.run(task=networking.napalm_get, name="Get SNMP information",
             getters=["get_snmp_information"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_trustsec_env(task, task_progress_bar: tqdm,
                         task_logging_level=logging.INFO):
    """Get TrustSec environment data
    !Supported on Cisco only
    """
    tqdm.write(f"{task.host}: getting TrustSec environment data...")

    task.run(task=networking.napalm_get,
             name="Get TrustSec environment-data",
             getters=["get_cts_environment_data"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_trustsec_PAC_keys(task, task_progress_bar: tqdm,
                          task_logging_level=logging.INFO):
    """Get TrustSec PAC keys
    !Supported on Cisco only
    """
    tqdm.write(f"{task.host}: getting TrustSec PAC keys...")

    task.run(task=networking.napalm_get,
             name="Get TrustSec PAC keys",
             getters=["get_cts_pacs"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_trustsec_servers(task, task_progress_bar: tqdm,
                         task_logging_level=logging.INFO):
    """Get TrustSec servers configured on host
    !Supported on Cisco only
    """
    tqdm.write(f"{task.host}: getting TrustSec servers...")

    task.run(task=networking.napalm_get,
             name="Get TrustSec servers",
             getters=["get_cts_server_list"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_trustsec_sgt_map(task, task_progress_bar: tqdm,
                         task_logging_level=logging.INFO):
    """Get TrustSec SGT mapping
    !Supported on Cisco only
    """
    tqdm.write(f"{task.host}: getting TrustSec SGT mapping...")

    task.run(task=networking.napalm_get,
             name="Get TrustSec SGT mapping",
             getters=["get_cts_role_based_sgt_map_all"],
             severity_level=task_logging_level)
    
    task_progress_bar.update()

def get_users(task, task_progress_bar: tqdm,
              task_logging_level=logging.INFO):
    """Get users
    """
    tqdm.write(f"{task.host}: getting users...")

    task.run(task=networking.napalm_get, name="Get users",
             getters=["get_users"], severity_level=task_logging_level)
    
    task_progress_bar.update()

def add_radius_server(task, radius_servers: list,
                      template_task_progress_bar: tqdm,
                      config_task_progress_bar: tqdm, 
                      template_task_logging_level=logging.INFO,
                      config_task_logging_level=logging.INFO,
                      TrustSec=False):
    """Configure RADIUS server

    Arguments:
        TrustSec: if RADIUS servers support Cisco TrustSec
        radius_servers: list of dicts containing RADIUS servers attributes
            required dict keys:
                IOS: server_name, IP_addr, auth_port, acct_port,
                       key, tester_username, tester_password
                Legacy IOS: IP_addr, auth_port, acct_port, key, 
                              tester_username, tester_password
        example:
        [{ "server_name": "ISE_1", "IP_addr": "10.10.10.100",
            "auth_port": "1812", "acct_port": "1813", 
            "key": "xyz", "tester_username": "test_user", 
            "tester_password": "xyz", ...}, ...]
    """
    # write provided RADIUS servers to inventory
    task.host["radius_servers"] = radius_servers
    # render inventory data to configuration via a template file
    # if platform IOS get the IOS version to use correct template to
    # build correct configuration
    
    os_version = 0 # needed for legacy Cisco IOS
    if task.host.platform == "ios":
        result = task.run(task=networking.napalm_get, 
                          name="Get IOS main version",
                          getters=["get_os_main_version"],
                          severity_level=logging.NOTSET) # no logging enabled
        os_version = result[0].result["get_os_main_version"][0]["VERSION"]
    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
        r = task.run(task=text.template_file,
            name="Building radius server configuration",
            template="add_radius_server.j2",
            path=f"configuration_templates/{task.host.platform}/legacy",
            severity_level=template_task_logging_level)
    elif TrustSec==True:
        r = task.run(task=text.template_file,
        name="Building radius server TrustSec configuration",
        template="add_radius_server_TrustSec.j2",
        path=f"configuration_templates/{task.host.platform}",
        severity_level=template_task_logging_level)
    else:           
        r = task.run(task=text.template_file,
            name="Building radius server configuration",
            template="add_radius_server.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)
    
    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"add RADIUS servers\" configuration built")

    # save the compiled configuration into host variable and tmp file
    # inventory host variable can be used to deploy config using SCP only
    # file can be used to deploy config using either SCP or inline transfer
    task.host["radius_server_config"] = r.result
    tmp_file = _create_tmp_file(r.result)

    # deploy the configuration
    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add RADIUS servers\" configuration...")
    task.run(task=networking.napalm_configure,
            name="Loading Configuration on the device",
            replace=False,
            #configuration=task.host["radius_server_config"],
            filename=tmp_file,
            severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def remove_radius_server(task, radius_servers: list, 
                         template_task_progress_bar: tqdm,
                         config_task_progress_bar: tqdm,
                         template_task_logging_level=logging.INFO,
                         config_task_logging_level=logging.INFO):
    """Remove RADIUS server

    Arguments:
        radius_servers: list of dicts containing RADIUS server 
        atrributes
            required dict keys:
                IOS: server_name
                Legacy IOS: IP_addr
        example: [{ "server_name": "ISE_1", 
                    "IP_addr": "10.10.10.100", ...}, ...]
    """
    task.host["radius_servers_to_remove"] = radius_servers

    os_version = 0 # needed for legacy Cisco IOS
    if task.host.platform == "ios":
        result = task.run(task=networking.napalm_get, 
                          name="Get IOS main version",
                          getters=["get_os_main_version"],
                          severity_level=logging.NOTSET) # no logging enabled
        os_version = result[0].result["get_os_main_version"][0]["VERSION"]
    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
        r = task.run(task=text.template_file,
                name="Building radius server configuration",
                template="remove_radius_server.j2",
                path=f"configuration_templates/{task.host.platform}/legacy",
                severity_level=template_task_logging_level)
    else:           
        r = task.run(task=text.template_file,
            name="Building radius server configuration",
            template="remove_radius_server.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)

    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"remove radius servers\" configuration built")

    task.host["radius_server_remove_config"] = r.result
    tmp_file = _create_tmp_file(r.result)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deplyoing \"remove radius servers\" configuration...")
    task.run(task=networking.napalm_configure,
            name="Loading Configuration on the device",
            replace=False,
            #configuration=task.host["radius_server_remove_config"],
            filename=tmp_file,
            severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def add_radius_dynamic_auth(task, radius_servers: list, 
                            template_task_progress_bar: tqdm, 
                            config_task_progress_bar: tqdm,
                            template_task_logging_level=logging.INFO,
                            config_task_logging_level=logging.INFO):
    """Configure radius dynamic auth/Change of Authorization (CoA)

    Arguments:
        radius_servers: list of dicts containing RADIUS servers attributes
            required dict keys:
                Cisco IOS: IP_addr, key
        example:
        [{ IP_addr: "10.10.20.100, key: "qazxsw", ...}, ...]  
    """
    task.host["radius_servers"] = radius_servers
          
    r = task.run(task=text.template_file,
                 name="Building radius server configuration",
                 template="add_radius_dynamic_auth.j2",
                 path=f"configuration_templates/{task.host.platform}",
                 severity_level=template_task_logging_level)

    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"add RADIUS dynamic auth\" configuration built")

    task.host["radius_dynamic_author_config"] = r.result
    tmp_file = _create_tmp_file(r.result)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add RADIUS dynamic auth\" configuration...")
    task.run(task=networking.napalm_configure,
             name="Loading Configuration on the device",
             replace=False,
             #configuration=task.host["radius_dynamic_author_config"],
             filename=tmp_file,
             severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def remove_radius_dynamic_auth(task, radius_servers: list,
                               template_task_progress_bar: tqdm,
                               config_task_progress_bar: tqdm,
                               template_task_logging_level=logging.INFO,
                               config_task_logging_level=logging.INFO,
                               get_task_logging_level=logging.INFO):
    """Remove radius dynamic auth/Change of Authorization (CoA)

    Arguments:
        radius_servers: list of dicts containing RADIUS servers IP addresses
            required dict keys:
                Cisco IOS: IP_addr
        example: [{ IP_addr: "10.10.20.100", ...}, ...]  
    """
    task.host["radius_servers"] = radius_servers
         
    r = task.run(task=text.template_file,
                 name="Building radius server configuration",
                 template="remove_radius_dynamic_auth.j2",
                 path=f"configuration_templates/{task.host.platform}",
                 severity_level=template_task_logging_level)

    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"remove radius dynamic auth\" configuration built")

    task.host["remove_radius_dynamic_author_config"] = r.result
    tmp_file = _create_tmp_file(r.result)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"remove radius dynamic auth\" configuration...")
    task.run(task=networking.napalm_configure,
             name="Loading Configuration on the device",
             replace=False,
             #configuration=task.host["remove_radius_dynamic_author_config"],
             filename=tmp_file,
             severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def add_radius_server_to_group(task, radius_server_group: str,
                               radius_servers: list,
                               template_task_progress_bar: tqdm,
                               config_task_progress_bar: tqdm,
                               template_task_logging_level=logging.INFO,
                               config_task_logging_level=logging.INFO):
    """Add RADIUS servers to RADIUS server group

    Arguments:
        radius_server_group: RADIUS group name
        radius_servers: list of dicts containing RADIUS servers:
            required dict keys:
                Cisco IOS: server_name
                Legacy Cisco IOS: IP_addr, auth_port, acct_port
            example: 
            [{"server_name": "ISE_1", "IP_addr": "10.10.20.3",
            "auth_port": "1812", "acct_port": "1813", ...}, ...]
    """

    task.host["radius_server_group"] = radius_server_group
    task.host["radius_servers"] = radius_servers
    server_names=[] # to display servers names in task progress console
    os_version = 0 # needed for legacy Cisco IOS
    if task.host.platform == "ios":
        result = task.run(task=networking.napalm_get,
                          name="Get IOS main version",
                          getters=["get_os_main_version"],
                          severity_level=logging.NOTSET)
        os_version = result[0].result["get_os_main_version"][0]["VERSION"]
    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
        r = task.run(task=text.template_file,
                name="Building radius group server configuration",
                template="add_radius_server_to_group.j2",
                path=f"configuration_templates/{task.host.platform}/legacy",
                severity_level=template_task_logging_level)
        for server in radius_servers:
            server_names.append(server["IP_addr"])
    else:
        r = task.run(task=text.template_file,
                name="Building radius group server configuration",
                template="add_radius_server_to_group.j2",
                path=f"configuration_templates/{task.host.platform}",
                severity_level=template_task_logging_level)
        for server in radius_servers:
                server_names.append(server["server_name"])

    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"add RADIUS servers to RADIUS group\" configuration built")

    task.host["radius_group_server_config"] = r.result
    config_file = _create_tmp_file(r.result)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add RADIUS servers to RADIUS group\" configuration...")
    task.run(task=networking.napalm_configure,
             name="Loading Configuration on the device",
             replace=False,
             #configuration=task.host["radius_group_server_config"],
             filename=config_file,
             severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def remove_radius_server_from_group(task, radius_server_group: str, 
                                    radius_servers: list,
                                    template_task_progress_bar: tqdm,
                                    config_task_progress_bar: tqdm, 
                                    template_task_logging_level=logging.INFO,
                                    config_task_logging_level=logging.INFO):
    """Remove RADIUS servers from RADIUS server group

    Arguments:
        radius_server_group: RADIUS group name
        radius_servers: list of dicts containing RADIUS servers:
            required dict keys:
                Cisco IOS: server_name
                Legacy Cisco IOS: IP_addr
        example: 
            [{"server_name": "ISE_1", "IP_addr": "10.10.20.3",
               ...}, ...]
    """
    task.host["radius_server_group"] = radius_server_group
    task.host["radius_servers_to_remove"] = radius_servers
    server_names=[] # to display servers names in task progress console
    os_version = 0 # needed for legacy Cisco IOS
    if task.host.platform == "ios":
        result = task.run(task=networking.napalm_get,
                          name="Get IOS main version",
                          getters=["get_os_main_version"],
                          severity_level=logging.NOTSET)

        os_version = result[0].result["get_os_main_version"][0]["VERSION"]
    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
        r = task.run(task=text.template_file,
                name="Building radius group server configuration",
                template="remove_radius_server_from_group.j2",
                path=f"configuration_templates/{task.host.platform}/legacy",
                severity_level=template_task_logging_level)
        for server in radius_servers:
            server_names.append(server["IP_addr"])
    else:
        r = task.run(task=text.template_file,
            name="Building radius group server configuration",
            template="remove_radius_server_from_group.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)
        for server in radius_servers:
                server_names.append(server["server_name"])

    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"remove RADIUS servers from RADIUS group\" configuration built")

    task.host["radius_server_remove_from_group_config"] = r.result
    config_file = _create_tmp_file(r.result)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"remove RADIUS servers from RADIUS group\" configuration...")
    task.run(task=networking.napalm_configure,
             name="Loading Configuration on the device",
             replace=False,
             #configuration=task.host["radius_group_server_config"],
             filename=config_file,
             severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def bounce_interface(task, interface_list: list,
                     template_task_progress_bar: tqdm, 
                     config_task_progress_bar: tqdm,
                     template_task_logging_level=logging.INFO,
                     config_task_logging_level=logging.INFO):
    """Disable and enable interface back

    Arguments:
        interface_list: list of interfaces to be bounced
        example: ["Gi1/2, "Fa0/0", "eth1/2", ...]
    """
 
    task.host["interfaces_to_be_bounced"] = interface_list

    r = task.run(task=text.template_file,
                 name="Building interface shutdown config",
                 template="shutdown_interface.j2",
                 path=f"configuration_templates/{task.host.platform}",
                 severity_level=template_task_logging_level)
        
    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"bounce interface\" (interface shutdown) configuration built")

    task.host["shutdown_interface_config"] = r.result
    tmp_file = _create_tmp_file(r.result)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"bounce interface\" (inerface shutdown) configuration...")
    if interface_list:
        task.run(task=networking.napalm_configure,
                name="Loading Configuration on the device",
                replace=False,
                #configuration=task.host["radius_server_config"],
                filename=tmp_file,
                severity_level=config_task_logging_level)

    config_task_progress_bar.update()

    r = task.run(task=text.template_file,
                 name="Building interface no shutdown config",
                 template="no_shutdown_interface.j2",
                 path=f"configuration_templates/{task.host.platform}",
                 severity_level=template_task_logging_level)

    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"bounce interface\" (interface no shutdown) configuration built")

    task.host["no_shutdown_interface_config"] = r.result
    tmp_file = _create_tmp_file(r.result)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"bounce interface\" (interface no shutdown) configuration ...")
    if interface_list:
        task.run(task=networking.napalm_configure,
                 name="Loading Configuration on the device",
                 replace=False,
                 #configuration=task.host["radius_server_config"],
                 filename=tmp_file,
                 severity_level=config_task_logging_level)
    else:
        task.run(task=data.echo_data,
                 name="No interfaces to bounce",
                 severity_level=config_task_logging_level)

    config_task_progress_bar.update()


def clear_auth_sessions(task, interface_list: list,
                        template_task_progress_bar: tqdm,
                        config_task_progress_bar: tqdm,
                        template_task_logging_level=logging.INFO,
                        config_task_logging_level=logging.INFO):
    """Clear authentication sessions (802.1x/MAB) on provided interfaces
    
    Arguments:
        interface_list: list of interfaces to be auth cleared
        example: ["Gi1/2, "Fa0/0", "eth1/2", ...]
    """
    task.host["interfaces_to_clear"] = interface_list
    
    r = task.run(task=text.template_file,
                 name="Building \"clear auth sessions\" commands",
                 template="clear_auth_sessions.j2",
                 path=f"configuration_templates/{task.host.platform}",
                 severity_level=template_task_logging_level)
                
    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"clear auth sessions\" commands built")               
    
    # delete last empty entry
    cmd_list = r.result.split("\n") 
    del cmd_list[-1]

    # napalm_cli doesn't work with dry_run/test mode by default therefore
    # if statement is used to run commands if dry_run/test mode disabled

    tqdm.write(f"{Fore.YELLOW}{task.host}: clearing authentication sessions...")
    if task.nornir.data.dry_run == False and cmd_list:
        task.run(task=networking.napalm_cli,
                name="Clearing authentication sessions",
                commands=cmd_list,
                severity_level=config_task_logging_level)
    else: # print the commands that would run if dry_run was disabled
        task.run(task=data.echo_data,
                 name="Clearing authentication sessions",
                 commands=cmd_list,
                 severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def clear_TrustSec_env_data(task,
                            template_task_progress_bar: tqdm,
                            config_task_progress_bar: tqdm,
                            template_task_logging_level=logging.INFO,
                            config_task_logging_level=logging.INFO):
    """Clear cts/TrustSec environment data. 
    Useful to force reconnecting to the servers
    Supported only on Cisco
    """

    tqdm.write(f"{Fore.YELLOW}{task.host}: clearing TrustSec environment data...")
    if task.nornir.data.dry_run == False:
        task.run(task=networking.napalm_cli,
                name="Clearing TrustSec environment data",
                commands=["clear cts environment-data"],
                severity_level=config_task_logging_level,)
    else:
        task.run(task=data.echo_data,
                 name="Clearing authentication sessions",
                 commands="clear cts environment-data",
                 severity_level=config_task_logging_level)
    template_task_progress_bar.update()
    config_task_progress_bar.update()

def delete_files(task, file_list: list, template_task_progress_bar: tqdm, 
                 config_task_progress_bar: tqdm, 
                 template_task_logging_level=logging.INFO,
                 config_task_logging_level=logging.INFO):
    """Delete files specified in file_list

    Arguments:
        file_list: list of files to be deleted
        example: ["merge_config", "test.log", ...]

    """
    # discover flash name
    r = task.run(task=networking.napalm_get, getters=["get_flash"],
                 severity_level=logging.NOTSET)
    
    task.host["files_to_delete"] = file_list
    task.host["flash_name"] = r[0].result["get_flash"]["flash_name"]
    
    r = task.run(task=text.template_file,
                 name="Building delete files commands",
                 template="delete_file.j2",
                 path=f"configuration_templates/{task.host.platform}",
                 severity_level=template_task_logging_level)
    
    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"delete files\" commands built")
    
    cmd_list = r.result.split("\n") 
    del cmd_list[-1]

    tqdm.write(f"{Fore.YELLOW}{task.host}: deleting {file_list} files...")
    if task.nornir.data.dry_run == False:
        task.run(task=networking.napalm_cli,
                 name="Deleting files",
                 commands=cmd_list,
                 severity_level=config_task_logging_level)
    else:
        task.run(task=data.echo_data,
                 name="Deleting files",
                 commands=cmd_list,
                 severity_level=config_task_logging_level)

    config_task_progress_bar.update()

def enable_TrustSec_enforcement(task, config_task_progress_bar: tqdm,
                                config_task_logging_level=logging.INFO):
    """Cisco TrustSec supported devices only
    """

    command = "cts role-based enforcement"
    tmp_file = _create_tmp_file(command)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"enable TrustSec enforcement\" configuration...")
    task.run(task=networking.napalm_configure,
             name="Disable TrustSec enforcement",
             replace=False,
             #configuration=command,
             filename=tmp_file,
             severity_level=config_task_logging_level)
    
    config_task_progress_bar.update()

def disable_TrustSec_enforcement(task, config_task_progress_bar: tqdm,
                                 config_task_logging_level=logging.INFO):
    """!Cisco TrustSec supported devices only!
    
    Disable TrustSec enforcement
    """

    command = "no cts role-based enforcement\n no cts role-based enforcement vlan-list"
    tmp_file = _create_tmp_file(command)

    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"disable TrustSec enforcement\" configuration")
    task.run(task=networking.napalm_configure,
             name="Disable TrustSec enforcement",
             replace=False,
             #configuration=command,
             filename=tmp_file,
             severity_level=config_task_logging_level)
    
    config_task_progress_bar.update()

def replace_config(task, config: str, config_type: str,
                   config_task_progress_bar: tqdm, 
                   config_task_logging_level=logging.INFO):
    """Replace [config_type] configuration with provided config

    Arguments:            
        config_type: [running|startup|candidate]
        config: configuration to be applied
    """

    # if inline transer is being used raise an error as replace config works
    # only with SCP (coded (ASCII) characters issue)
    if task.host.defaults.connection_options["napalm"].extras["optional_args"]["inline_transfer"] == True:
        raise (Exception("Replace config doesn't work with inline transfer. Please use SCP (Secure Copy)"))
    
    task.host["config"] = {}
    task.host["config"][config_type] = config
    #if Cisco IOS delete first few lines from the config
    if task.host.platform == "ios":
        # replace the lines with "!"
        task.host["config"][config_type] = re.sub(
            r"^Building configuration...\n",
            "!",
            task.host["config"][config_type])
        task.host["config"][config_type] = re.sub(
            r"Current configuration : \d+ bytes\n",
            "!",
            task.host["config"][config_type])
        # task.host[config][config_type] = task.host[config][config_type].encode("utf-8")
        # IOS requires that the banner character use the ETX character(ASCII 3)
        task.host["config"][config_type] = re.sub(r"\^C+", chr(3),
                                            task.host["config"][config_type])

    tqdm.write(f"{Fore.YELLOW}{task.host}: replacing configuration...")
    task.run(task=networking.napalm_configure,
             name="Replacing configuration",
             replace=True,
             configuration=task.host["config"][config_type],
             severity_level=config_task_logging_level)
    config_task_progress_bar.update()

def rollback(task, comment="", taskname="rollback",
             task_logging_level=logging.INFO):
    """Rollback configuration

    Arguments:
        comment: comment to display
        taskname: task name
    """

    tqdm.write(f"{task.host}: rolling back configuration...")

    if comment != "":
        task.run(task=data.echo_data,
                 name=taskname,
                 rollback_reason=comment,
                 severity_level=task_logging_level)
    
    if task.nornir.data.dry_run == False:
        device = task.host.get_connection("napalm", task.nornir.config)
        device.rollback()

    tqdm.write(f"{task.host}: configuration rolled back")

def test_radius_user_auth(task, user: str, password: str,
                          radius_server_name: str, radius_server_ip: str,
                          validate_task_progress_bar: tqdm,
                          template_task_progress_bar: tqdm,
                          validate_task_logging_level=logging.INFO,
                          template_task_logging_level=logging.INFO):
    """Test user authentication against RADIUS server

    Arguments:
        user: username to test
        password: username password
        radius_server_name (IOS): server to test the authentication for
        radius_server_ip (Legacy IOS): server IP to test the authentication for
    """
    task.host["radius_user_to_test"] = user
    task.host["radius_user_password_to_test"] = password
    task.host["radius_server_name_to_test"] =  radius_server_name
    task.host["radius_server_ip_to_test"] =  radius_server_ip

    #if platform IOS get the IOS version to use correct template and to build\
    # correct configuration
    os_version = 0 # needed for legacy Cisco IOS
    if task.host.platform == "ios":
        result = task.run(task=networking.napalm_get,
                          name="Get IOS main version",
                          getters=["get_os_main_version"],
                          severity_level=logging.NOTSET)
        os_version = result[0].result["get_os_main_version"][0]["VERSION"]

    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
        r = task.run(task=text.template_file,
                name="Building test RADIUS user auth command",
                template="test_radius_user_auth.j2",
                path=f"configuration_templates/{task.host.platform}/legacy",
                severity_level=template_task_logging_level)
    else:
        r = task.run(task=text.template_file,
            name="Building test RADIUS user auth command",
            template="test_radius_user_auth.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)

    template_task_progress_bar.update()
    tqdm.write(f"{task.host}: \"test RADIUS user auth\" command built")

    cmd = r.result
    tqdm.write(f"{Fore.GREEN}{task.host}: validating test RADIUS user authentication ...") 
    if task.nornir.data.dry_run == False:
        r = task.run(task=networking.napalm_get,
                    name=f"Test RADIUS user auth",
                    getters=["test_radius_user_auth"],
                    command=cmd,
                    user=user,
                    server_name=radius_server_name,
                    severity_level=validate_task_logging_level)
        task.host["test_radius_user_auth_result"] = r.result["test_radius_user_auth"]
        if r.result["test_radius_user_auth"]["authenticated"] == False:
            tqdm.write(f"{Fore.RED}{task.host}: RADIUS test user auth failed")
    else:
        task.run(task=data.echo_data,
                 name="Validating RADIUS user authentication",
                 commands=cmd,
                 severity_level=validate_task_logging_level)        
        
    validate_task_progress_bar.update()

def _create_tmp_file(config: str):
    """Write config to a temp file. Required if inline transfer is used
    """
    tmp_dir = tempfile.gettempdir()
    rand_fname = py23_compat.text_type(uuid.uuid4())
    filename = os.path.join(tmp_dir, rand_fname)
    with open(filename, "wt") as fobj:
        fobj.write(config)
    return filename

def _create_file(config: str, filename: str, path=""):
    """Save config to a file
    """ 
    # create folder if not exists
    if path!="":
        if not os.path.exists(path):
            os.mkdir(path)

    new_filename = path + filename
    with open(new_filename, "wt") as fobj:
        fobj.write(config)
    return new_filename

def validate(task, task_progress_bar: tqdm, data_to_validate,
             tqdm_message="validating data...", task_logging_level=logging.INFO):
    """Validate provided data

    Arguments:
        data_to_validate: list of dicts containing getters and data to validate
        example: [{"get_aaa_servers": radius_servers_dict}, ...]

        message: message to be displayed by tqdm.write  to task progress console
    
    Help: Run getter first to see the format of getter return data
    """
    tqdm.write(f"{Fore.GREEN}{task.host}: {tqdm_message}")
    task.run(task=networking.napalm_validate,
             name="Validate data",
             validation_source=data_to_validate,
             severity_level=task_logging_level)
    task_progress_bar.update() 

def keep_alive(task):
    napalm = task.host.get_connection("napalm", task.nornir.config)
    napalm.is_alive()

def migrate_radius_servers(task, existing_radius_servers: list, CoA_servers: list,
                           existing_radius_server_group: str, 
                           new_radius_servers: list,
                           test_user: str,
                           test_user_password: str,
                           get_task_progress_bar: tqdm,
                           validate_task_progress_bar: tqdm,
                           template_task_progress_bar: tqdm,
                           config_task_progress_bar: tqdm, 
                           template_task_logging_level=logging.INFO,
                           config_task_logging_level=logging.INFO,
                           get_task_logging_level=logging.DEBUG,
                           validate_task_logging_level=logging.INFO,
                           validate_session_task_logging_level=logging.DEBUG):
    """ Configure new RADIUS servers, add them to the existing RADIUS
    server group and to CoA list.
    Validate if provided RADIUS servers and group exist and are up.
    Rollback automatically if any validation fails.

    Arguments:
        
        exisiting_radius_servers: list of dicts containing RADIUS servers
            required dict keys:
                IOS: server_name, IP_addr
                Legacy IOS: IP_addr
        example: [{"server_name": "ISE_1", "IP_addr": "10.1.1.3", ...}, ...]
        
        CoA_servers: list of dicts containing RADIUS servers IPs and keys
            required dict keys:
                IOS: IP_addr, key
        example: [{ IP_addr: "10.10.10.100, key: "qazxsw", ...}, ...]
        
        existing_radius_server_group: name of existing RADIUS server group

        new_radius_servers: list of dict containing RADIUS servers to configure
            required dict keys:
                IOS: server_name, IP_addr, auth_port, acct_port, 
                     key
                Legacy IOS: IP_addr, auth_port, acct_port, key
        example:
        [{ "server_name": "ISE_1", "IP_addr": "10.10.10.100", 
            "auth_port": "1812", "acct_port": "1813",
            "key": "xyz", ...}, ...] 

        test_user: username to test auth for new servers
        test_user_password: test user password   
    """
    tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS servers migration script's started")
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    # get running config and save to file
    task.run(task=get_config, dest_config_name="config_before_migration",
             task_progress_bar=get_task_progress_bar,
             task_logging_level=logging.NOTSET,
             tqdm_message="getting configuration and save to file...",
             name=f"Get config and save to file \"{task.host.name}_{timestamp}_1_before_migration.conf\"")
    _create_file(config=task.host["config_before_migration"]["running"], 
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_1_running_config_before_migration.conf")
    
    # get other information and save to files 
    tqdm.write(f"{task.host}: getting MAC address table, IP device tracking table, auth sessions and save to files...")    
    result = task.run(task=networking.napalm_get,
                      getters=["get_mac_address_table",
                               "get_ip_device_tracking_all",
                               "get_auth_sessions"],
                      severity_level=get_task_logging_level,
                      name="Get MAC address table, IP device tracking table, auth sessions and save to files")
    if get_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Get MAC address table, IP device tracking table, auth sessions and save to files")
    
    task.run(task=data.echo_data,
             name=f"Save MAC address table to file \"{task.host.name}_{timestamp}_2_MAC_table_before_migration.pydict\"")
    _create_file(config=str(result.result["get_mac_address_table"]), 
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_2_MAC_table_before_migration.pydict")
    task.run(task=data.echo_data,
             name=f"Save IP device tracking table to file \"{task.host.name}_{timestamp}_2_IP_device_tracking_table_before_migration.pydict\"")
    _create_file(config=str(result.result["get_ip_device_tracking_all"]),
                 filename=f"{task.host.name}_{timestamp}_2_IP_device_tracking_table_before_migration.pydict", path="migrate_radius_servers/")
    task.run(task=data.echo_data,
             name=f"Save auth sessions to file \"{task.host.name}_{timestamp}_2_auth_sessions_before_migration.pydict\"")
    _create_file(config=str(result.result["get_auth_sessions"]),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_2_auth_sessions_before_migration.pydict")
    # save session table to host inventory to use it later for clear auth sessions
    task.host["auth_sessions_before_migration"] = result.result["get_auth_sessions"]
    get_task_progress_bar.update()


    # validate current configuration
        # if needed reformat data to be use with validate
    exist_servers_to_vld = {}
    for server in existing_radius_servers:
        exist_servers_to_vld[server["IP_addr"]] = {"STATE": "UP"}
    
    data_to_validate = [
        {
            "get_aaa_servers": exist_servers_to_vld
        },
        {
            "get_radius_groups": {
                "radius_group_list": {
                    "list": [existing_radius_server_group]
                }
            }
        }
    ]
    tqdm.write(f"{Fore.GREEN}{task.host}: validating existing RADIUS servers and group...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=data_to_validate,
                          name="Validate existing RADIUS servers and group",
                          severity_level=validate_task_logging_level)
    validate_task_progress_bar.update()

    # stop the task if validated data is not compliant
        # if provided servers are down or not exist
    if vld_result.result["get_aaa_servers"]["complies"] == False:
        dead_server_list = []
        for server, info in vld_result.result["get_aaa_servers"]["present"].items():
            if info["complies"] == False:
                dead_server_list.append(server)
        if dead_server_list:
            result = task.run(task=data.echo_data,
                        name = f"Provided radius servers: {str(dead_server_list).strip('[]')} are dead",
                        message= "Please fix the existing RADIUS servers first before running the migration task again",
                        severity_level=logging.ERROR)
            result[0].failed = True
            tqdm.write(f"{Fore.RED}{task.host}: provided RADIUS servers: {str(dead_server_list).strip('[]')} are down. Task aborted! Check logs for more info.")
        if vld_result.result["get_aaa_servers"]["missing"]:
            result=task.run(task=data.echo_data,
                        name = f"Provided radius servers: {str(vld_result.result['get_aaa_servers']['missing']).strip('[]')} are not configured",
                        message= "Please make sure provided details for the exisitng RADIUS servers are correct",
                        severity_level=logging.ERROR)
            result[0].failed = True
            tqdm.write(f"{Fore.RED}{task.host}: provided RADIUS servers: {str(vld_result.result['get_aaa_servers']['missing']).strip('[]')} not exist on the switch. Task aborted! Check logs for more info.")
        task.run(task=data.echo_data,
                 name = "ABORTING TASK!",
                 severity_level=logging.INFO)
        tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS server migration script finished")
        # exit task
        return

    # existing RADIUS servers are compliant
    task.run(task=data.echo_data,
        name = f"Provided RADIUS servers: {existing_radius_servers} are UP",
        severity_level=logging.INFO)

    # if provided RADIUS server group not exists
    if vld_result.result["get_radius_groups"]["complies"] == False:
        result=task.run(task=data.echo_data,
            name = f"RADIUS group {existing_radius_server_group} not exists.",
            message= "Please make sure you provided correct RADIUS group name",
            severity_level=logging.ERROR)
        result[0].failed = True
        tqdm.write(f"{Fore.RED}{task.host}: {existing_radius_server_group} RADIUS group not exists. Task aborted! Check logs for more info.")
        task.run(task=data.echo_data, name = "ABORTING TASK!",
                 severity_level=logging.INFO)
        tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS server migration script finished")
        # exit task
        return
    
    # existing RADIUS group is compliant
    task.run(task=data.echo_data,
             name=f"Provided radius group \"{existing_radius_server_group}\" exists",
             severity_level=logging.INFO)


    # build configuration using tempalates
    task.host["radius_servers"] = new_radius_servers
    task.host["radius_servers_CoA"] = CoA_servers
    task.host["test_user"] = test_user
    os_version = 0 # needed for legacy Cisco IOS
    if task.host.platform == "ios":
        result = task.run(task=networking.napalm_get, 
                          name="Get IOS main version",
                          getters=["get_os_main_version"],
                          severity_level=logging.NOTSET)
        os_version = result.result["get_os_main_version"][0]["VERSION"]
    # if device is legacy Cisco IOS
    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
            result = task.run(task=text.template_file,
                name="Build \"add RADIUS servers\" configuration",
                template="migrate_radius_server.j2",
                path=f"configuration_templates/{task.host.platform}/legacy",
                severity_level=template_task_logging_level)
    # if not use standard template per platform
    else:           
        result = task.run(task=text.template_file,
            name="Build \"add RADIUS servers\" configuration",
            template="migrate_radius_server.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)         

    # save built configuration to host inventory and to file
    task.host["radius_server_config"] = result.result
    config_file = _create_tmp_file(result.result)
    tqdm.write(f"{task.host}: \"add new RADIUS servers\" configuration built")
    template_task_progress_bar.update()


    # deploy built configuration
    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add new RADIUS servers\" configuration...")
    try:
        task.run(task=networking.napalm_configure,
                name="Loading Configuration on the device",
                replace=False,
                #configuration=task.host["radius_server_config"], # work only with SCP config transfer mode
                filename=config_file, # works both with SCP and Inline config transfer mode
                severity_level=config_task_logging_level)
    except NornirSubTaskError:
        tqdm.write(f"{Fore.RED}{task.host}: error during configuration deployment, automatic rollback attempted. Check provieded data and see logs for mode details.")
        tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS server migration script finished")
        return
    config_task_progress_bar.update()


    # validate if new servers are up by authenticating test user
    tqdm.write(f"{Fore.GREEN}{task.host}: validating new RADIUS servers by authenticating test user...")    
    # it is performed only when task is run with dry_run=False as new servers
    # have to be deployed first
    if task.nornir.data.dry_run==False:
        result = task.run(task=networking.napalm_get,
                          getters=["get_test_radius_user_auth2"],
                          getters_options={"get_test_radius_user_auth2": { 
                          "user": test_user, "password": test_user_password,
                          "servers": new_radius_servers}},
                          severity_level=validate_task_logging_level,
                          name="Validate test user auth for new RADIUS servers")
        for server, auth_result in result.result["get_test_radius_user_auth2"].items():
            if server != "username":
                if auth_result["authenticated"] == False:
                    comment = f"Failed user \"{test_user}\" authentication for new RADIUS server \"{auth_result['radius_server']}\", IP address \"{auth_result['radius_server_IP']}\"\
                        \nReason: {auth_result['reason']}"
                    echo_task = task.run(task=data.echo_data,
                                    name="Validate RADIUS test user authentication failed",
                                    message = comment,
                                    severity_level=logging.ERROR)
                    echo_task[0].failed = True
                    task.run(task=data.echo_data, name = "ABORTING TASK!")
                    tqdm.write(f"{Fore.RED}{task.host}: user authentication failed. Check provided data and see logs for more details.")
                    # rollback configuration
                    task.run(task=rollback, name="Rollback")
                    tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS server migration script finished")
                    validate_task_progress_bar.update()
                    # exit task
                    return
                else:
                    task.run(task=data.echo_data,
                            name=f"Test user \"{test_user}\" successfully authenticated on \"{auth_result['radius_server']}\", IP address \"{auth_result['radius_server_IP']}\"")
    else:
        task.run(task=data.echo_data,
                 name = "Validate test user auth for new RADIUS servers")
    validate_task_progress_bar.update()


    # clear auth seessions to have a fresh auth session table
    interface_to_be_auth_cleared_list = []
    for session in task.host["auth_sessions_before_migration"]:
        interface_to_be_auth_cleared_list.append(session["INTERFACE"])
    task.run(task=clear_auth_sessions,
             interface_list=interface_to_be_auth_cleared_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Clear authentication sessions before migration")    

    # waite to allow endpoints reauthenticate
    clear_auth_timeout = 2
    tqdm.write(f"{task.host}: waiting {clear_auth_timeout}m to allow endpoints to reauthenticate...")  
    if task.nornir.data.dry_run==False:
        for i in range(clear_auth_timeout):
            time.sleep(60)
            task.run(task=keep_alive,
                    severity_level=logging.INFO,
                    name="Keep connection up till endpoints reauthenticate") 
            if i+1 != clear_auth_timeout:
                tqdm.write(f"{task.host}: waiting {clear_auth_timeout}m to allow endpoints to reauthenticate - {clear_auth_timeout-i-1} minute(s) left")

    # get fresh auth sessions table and save to file and to host inventory
    result = task.run(task=get_auth_sessions,
                      tqdm_message="getting fresh auth sessions table and save to file...",
                      task_progress_bar=get_task_progress_bar,
                      task_logging_level=get_task_logging_level,
                      name=f"Get fresh auth sessions table and save to file \"{task.host.name}_{timestamp}_3_auth_sessions_after_1st_clear.pydict\"")
    _create_file(config=str(result[1].result["get_auth_sessions"]),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_3_auth_sessions_after_1st_clear.pydict")
    # save auth sessions table to host inventory to use it for validation after servers migration
    task.host["auth_sessions_after_1st_clear"] = result[1].result["get_auth_sessions"]

    # get fresh auth sessions details and save to file and to host inventory
    tqdm.write(f"{task.host}: getting auth dot1x and mab sessions details and save to files...")
    result = task.run(task=networking.napalm_get,
                      getters=["get_auth_sessions_dot1x_details",
                               "get_auth_sessions_mab_details"],
                      severity_level=get_task_logging_level,
                      name=f"Get fresh auth dot1x sessions  and save to file \"{task.host.name}_{timestamp}_3_auth_sessions_dot1x_details_after_1st_clear.pydict\"")
    _create_file(config=str(result.result["get_auth_sessions_dot1x_details"]),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_3_auth_sessions_dot1x_details_after_1st_clear.pydict")
    _create_file(config=str(result.result["get_auth_sessions_mab_details"]),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_3_auth_sessions_mab_details_after_1st_clear.pydict")
    get_task_progress_bar.update()

    # save auth  sessions details to host inventory to use it for validation after servers migration
    task.host["auth_dot1x_sessions_details_after_1st_clear"] = result.result["get_auth_sessions_dot1x_details"]
    task.host["auth_mab_sessions_details_after_1st_clear"] = result.result["get_auth_sessions_mab_details"]


    # add new RADIUS servers to RADIUS group (above old servers)
    # build configuration using tempalates to add new and remove old servers
    task.host["radius_server_group"] = existing_radius_server_group
    task.host["old_radius_servers"] = existing_radius_servers
    # if device is legacy Cisco IOS
    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
            result = task.run(task=text.template_file,
                name="Build \"add RADIUS servers to group (and remove old servers)\" configuration",
                template="migrate_radius_server_add_new_servers_to_group.j2",
                path=f"configuration_templates/{task.host.platform}/legacy",
                severity_level=template_task_logging_level)
    # if not, use standard per platform template
    else:           
        result = task.run(task=text.template_file,
            name="Build \"add RADIUS servers to group (and remove old servers)\" configuration",
            template="migrate_radius_server_add_new_servers_to_group.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)         

    # save built configuration to host inventory and to file
    task.host["radius_server_config"] = result.result
    config_file = _create_tmp_file(result.result)
    tqdm.write(f"{task.host}: \"add new RADIUS servers to RADIUS group (and remove old servers)\" configuration built")
    template_task_progress_bar.update()

    # deploy built configuration
    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add new RADIUS servers to RADIUS group (and remove old servers)\" configuration...")
    task.run(task=networking.napalm_configure,
            name="Loading Configuration on the device",
            replace=False,
            #configuration=task.host["radius_server_config"], # work only with SCP config transfer mode
            filename=config_file, # works both with SCP and Inline config transfer mode
            severity_level=config_task_logging_level)
    config_task_progress_bar.update()

    # add old RADIUS servers back to RADIUS group (below new servers) for global failover scenario
    # build configuration using tempalates
    # if device is legacy Cisco IOS
    if ((task.host.platform == "ios") and (int(os_version) <= 15) and (int(os_version) > 3)):
        if int(os_version) <= 15 and int(os_version) > 3:
            result = task.run(task=text.template_file,
                name="Build \"add old RADIUS servers to RADIUS group (below new servers)\" configuration",
                template="migrate_radius_server_add_old_servers_to_group.j2",
                path=f"configuration_templates/{task.host.platform}/legacy",
                severity_level=template_task_logging_level)
    # if not, use standard per platform template
    else:           
        result = task.run(task=text.template_file,
            name="Build \"add old RADIUS servers to RADIUS group (below new servers)\" configuration",
            template="migrate_radius_server_add_old_servers_to_group.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)         

    # save built configuration to host inventory and to file
    task.host["radius_server_config"] = result.result
    config_file = _create_tmp_file(result.result)
    tqdm.write(f"{task.host}: \"add old RADIUS servers to RADIUS group (below new servers)\" configuration built")
    template_task_progress_bar.update()

    # deploy built configuration
    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add old RADIUS servers to RADIUS group (below new servers)\" configuration...")
    task.run(task=networking.napalm_configure,
            name="Loading Configuration on the device",
            replace=False,
            #configuration=task.host["radius_server_config"], # work only with SCP config transfer mode
            filename=config_file, # works both with SCP and Inline config transfer mode
            severity_level=config_task_logging_level)
    config_task_progress_bar.update()

    time.sleep(1)
    # build list of interfaces to be auth cleared after migration
    # 802.1x auth interfaces are excluded as they need to be bounced to force Anyconnect restarts posture checking
    interface_to_be_auth_cleared_list = []
    for session in task.host["auth_sessions_after_1st_clear"]:
        if session["METHOD"] != "dot1x":
            interface_to_be_auth_cleared_list.append(session["INTERFACE"])
    task.run(task=clear_auth_sessions,
             interface_list=interface_to_be_auth_cleared_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Clear MAB authentication sessions after migration")

    # bounce 802.1x ports
    # build list of interface to be bounced  
    dot1x_interface_to_be_bounced_list = []
    for session in task.host["auth_sessions_after_1st_clear"]:
        if session["METHOD"] == "dot1x":
            dot1x_interface_to_be_bounced_list.append(session["INTERFACE"])
    # delete duplicate interfaces - multiple endpoints on one interface
    dot1x_interface_to_be_bounced_list = list(set(dot1x_interface_to_be_bounced_list))
    
    task.host["dot1x_interface_to_be_bounced_list"] = dot1x_interface_to_be_bounced_list

    tqdm.write(f"{task.host}: bouncing interfaces with 802.1x auth sessions")
    task.run(task=bounce_interface,
             interface_list=dot1x_interface_to_be_bounced_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Bounce 802.1x interfaces to restart Anyconnect posture")   

    # wait to allow endpoints authenticate to new servers
    port_bounce_auth_timeout = 5
    tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout} to allow endpoints to authenticate to new RADIUS servers...")
    if task.nornir.data.dry_run==False:
        for i in range(port_bounce_auth_timeout):
            time.sleep(60)
            task.run(task=keep_alive,
                    severity_level=logging.INFO,
                    name="Keep connection up till endpoints reauthenticate") 
            if i+1 != port_bounce_auth_timeout:
                tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout}m to allow endpoints to reauthenticate - {port_bounce_auth_timeout-i-1} minute(s) left")

    
    # get auth sessions table and save to file and to host inventory
    result = task.run(task=get_auth_sessions,
                      tqdm_message="getting auth sessions table (after migration) and save to file...",
                      task_progress_bar=get_task_progress_bar,
                      task_logging_level=get_task_logging_level,
                      name=f"Get auth sessions table and save to file \"{task.host.name}_{timestamp}_4_auth_sessions_after_migration.pydict\"")
    _create_file(config=str(result[1].result["get_auth_sessions"]),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_4_auth_sessions_after_migration.pydict")
    # save auth sessions table to host inventory to use it for validation after servers migration
    task.host["auth_sessions_after_migration"] = result[1].result["get_auth_sessions"]


    # comper sessions before and after migration
    for session in task.host["auth_sessions_after_1st_clear"]:
        del session["SESSION"]
    sessions_before_migration = [
        {
            "get_auth_sessions": {
                "_mode": "strict",
                "list": list(task.host["auth_sessions_after_1st_clear"])
                }
        }
    ]

    tqdm.write(f"{Fore.GREEN}{task.host}: validating authentication sessions...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=sessions_before_migration,
                          name="Validate for any missing endpoints after clear auth",
                          severity_level=validate_session_task_logging_level)
    validate_task_progress_bar.update()
    if validate_session_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Validate for any missing endpoints after clear auth")
 


    # bounce ports with missing endpoints (silent/no reauth host issue)
    # build list of interface to be bounced  
    interface_to_be_bounced_list = []
    for session in vld_result.result["get_auth_sessions"]["missing"]:
        interface_to_be_bounced_list.append(session["INTERFACE"])
    # delete duplicate interfaces - multiple endpoints on one interface
    interface_to_be_bounced_list = list(set(interface_to_be_bounced_list))
    
    task.host["interfaces_to_be_bounced"] = interface_to_be_bounced_list

    tqdm.write(f"{task.host}: bouncing interfaces with missing auth sessions")
    task.run(task=bounce_interface,
             interface_list=interface_to_be_bounced_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Bounce interfaces that have missing auth endpoints")


    # wait to allow missing endpoints to authenticate to new servers
    port_bounce_auth_timeout = 5
    tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout}m to allow missing endpoints to authenticate...")
    if (interface_to_be_bounced_list and (task.nornir.data.dry_run==False)):
        for i in range(port_bounce_auth_timeout):
            time.sleep(60)
            task.run(task=keep_alive,
                    severity_level=logging.INFO,
                    name="Keep connection up till endpoints reauthenticate") 
            if i+1 != port_bounce_auth_timeout:
                tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout}m to allow missing endpoints to authenticate - {port_bounce_auth_timeout-i-1} minute(s) left")


    # get auth sessions after auth cleared and ports bounced and save to file
    result = task.run(task=get_auth_sessions,
                      tqdm_message="getting auth sessions table after bouncing ports and save to file...",
                      task_progress_bar=get_task_progress_bar,
                      task_logging_level=get_task_logging_level,
                      name=f"Get auth sessions table and save to file \"{task.host.name}_{timestamp}_5_auth_sessions_after_port_bounce.pydict\"")
    _create_file(config=str(result[1].result["get_auth_sessions"]),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_5_auth_sessions_after_port_bounce.pydict")
    # save auth sessions table to host inventory to use it wiht validate for missing endpoints
    task.host["auth_sessions_after_port_bounce"] = result[1].result["get_auth_sessions"]

    # get missing session after migration
    tqdm.write(f"{Fore.GREEN}{task.host}: validating missing authentication sessions and save to file...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=sessions_before_migration,
                          name=f"Validate missing auth endpoints after port bounce and save to \"{task.host.name}_{timestamp}_6_missing_auth_endpoints_after_migration.pydict\"",
                          severity_level=validate_session_task_logging_level)
    validate_task_progress_bar.update()
    if validate_session_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Validate for any missing endpoints after clear auth")
    
    _create_file(config=str(vld_result.result["get_auth_sessions"]["missing"]),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_6_missing_auth_endpoints_after_migration.pydict")

    # get auth sessions details that change the autorization after migration
    for key in task.host["auth_dot1x_sessions_details_after_1st_clear"]:
            # delete unique data
            del task.host["auth_dot1x_sessions_details_after_1st_clear"][key]["SESSION"]
            del task.host["auth_dot1x_sessions_details_after_1st_clear"][key]["ACS_ACL"]
            del task.host["auth_dot1x_sessions_details_after_1st_clear"][key]["URL_REDIRECT"]
    for key in task.host["auth_mab_sessions_details_after_1st_clear"]:
            del task.host["auth_mab_sessions_details_after_1st_clear"][key]["SESSION"]
            del task.host["auth_mab_sessions_details_after_1st_clear"][key]["ACS_ACL"]
            del task.host["auth_mab_sessions_details_after_1st_clear"][key]["URL_REDIRECT"]
    sessions_details_before_migration = [
        {
            "get_auth_sessions_dot1x_details": task.host["auth_dot1x_sessions_details_after_1st_clear"],
            "get_auth_sessions_mab_details": task.host["auth_mab_sessions_details_after_1st_clear"]

        }
    ]
    tqdm.write(f"{Fore.GREEN}{task.host}: validating authentication session details and save to file...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=sessions_details_before_migration,
                          name=f"Validate auth session details and save to \"{task.host.name}_{timestamp}_6_auth_sessions_details_after_migration.pydict\"",
                          severity_level=validate_session_task_logging_level)
    validate_task_progress_bar.update()
    if validate_session_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Validate auth session details and save to \"{task.host.name}_{timestamp}_6_auth_sessions_details_after_migration.pydict\"")
 
    _create_file(config=str(vld_result.result),
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_6_auth_sessions_details_after_migration.pydict")

    # get IP device tracking table and MAC address table and save to files 
    tqdm.write(f"{task.host}: getting MAC address table, IP device tracking table and save to files...")    
    result = task.run(task=networking.napalm_get,
                      getters=["get_mac_address_table",
                               "get_ip_device_tracking_all"],
                      severity_level=get_task_logging_level,
                      name="Get MAC address table, IP device tracking table and save to files")
    if get_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Get MAC address table, IP device tracking table and save to files")
    task.run(task=data.echo_data,
             name=f"Save MAC address table to file \"{task.host.name}_{timestamp}_7_MAC_table_after_migration.pydict\"")
    _create_file(config=str(result.result["get_mac_address_table"]), 
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_7_MAC_table_after_migration.pydict")
    task.run(task=data.echo_data,
             name=f"Save IP device tracking table to file \"{task.host.name}_{timestamp}_7_IP_device_tracking_table_after_migration.pydict\"")
    _create_file(config=str(result.result["get_ip_device_tracking_all"]),
                 filename=f"{task.host.name}_{timestamp}_7_IP_device_tracking_table_after_migration.pydict", path="migrate_radius_servers/")
    get_task_progress_bar.update()
    
    # get running config and save to file
    task.run(task=get_config, task_progress_bar=get_task_progress_bar,
             dest_config_name="config_after_migration",
             task_logging_level=logging.NOTSET,
             tqdm_message="getting configuration and save to file...",
             name=f"Get run config and save to \"{task.host.name}_{timestamp}_8_running_config_after_migration.conf\" file")    
    _create_file(config=task.host["config_after_migration"]["running"],
                 path="migrate_radius_servers/",
                 filename=f"{task.host.name}_{timestamp}_8_running_config_after_migration.conf")

    tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS server migration script finished")


def migrate_TrustSec_servers(task, existing_TrustSec_servers: list,
                             existing_radius_server_group: str,
                             new_TrustSec_servers: list,
                             CoA_servers: list,
                             test_user: str,
                             test_user_password: str,
                             get_task_progress_bar: tqdm,
                             validate_task_progress_bar: tqdm,
                             template_task_progress_bar: tqdm,
                             config_task_progress_bar: tqdm,
                             get_task_logging_level=logging.DEBUG,
                             validate_task_logging_level=logging.INFO,
                             template_task_logging_level=logging.INFO,
                             config_task_logging_level=logging.INFO,
                             validate_session_task_logging_level=logging.DEBUG):
    """Configure new TrustSec servers, add them to the existing RADIUS
    server group and to CoA list.
    Validate if provided TrustSec servers and group exist and are up.
    Rollback automatically if any validation fails.

    Arguments:
        
        exisiting_TrustSec_servers: list of dicts containing TrustSec servers
            required dict keys:
                IOS: server_name, IP_addr
        example: [{"server_name": "ISE_1", "IP_addr": "10.1.1.3", ...}, ...]

        existing_radius_server_group: name of existing RADIUS server group

        CoA_servers: list of dicts containing RADIUS servers IPs and keys
            required dict keys:
                IOS: IP_addr, key
        example: [{ IP_addr: "10.10.10.100, key: "qazxsw", ...}, ...]
        
        test_user: username to test authentication
        test_user_password: test user password 

        new_TrustSec_servers: list of dict containing TrustSec servers to configure
            required dict keys:
                IOS: server_name, IP_addr, auth_port, acct_port, key
        example:
        [{ "server_name": "ISE_1", "IP_addr": "10.10.10.100", 
            "auth_port": "1812", "acct_port": "1813",
            "key": "xyz", ...}, ...]       
    """
    
    
    tqdm.write(f"{Fore.BLUE}{task.host}: TrustSec migration script's started")
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    # get running config and save to file
    task.run(task=get_config, dest_config_name="config_before_TrustSec_migration",
             task_progress_bar=get_task_progress_bar,
             task_logging_level=logging.NOTSET,
             tqdm_message="getting configuration and save to file...",
             name=f"Get run config and save to file \"{task.host.name}_{timestamp}_1_before_TrustSec_migration.conf\"")
    _create_file(config=task.host["config_before_TrustSec_migration"]["running"], 
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_1_running_config_before_TrustSec_migration.conf")

    # get other information and save to files 
    tqdm.write(f"{task.host}: getting MAC address table, TrustSec data, PAC keys, IP device tracking table, auth sessions and save to files...")    
    result = task.run(task=networking.napalm_get,
                      getters=["get_mac_address_table",
                               "get_cts_server_list",
                               "get_cts_environment_data",
                               "get_cts_role_based_sgt_map_all",
                               "get_cts_pacs",
                               "get_ip_device_tracking_all",
                               "get_auth_sessions"],
                      severity_level=get_task_logging_level,
                      name="Get MAC address table, TrustSec data, PAC keys, IP device tracking table, auth sessions and save to files")
    if get_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Get MAC address table, TrustSec data, PAC keys, IP device tracking table, auth sessions and save to files")
    
    task.run(task=data.echo_data,
             name=f"Save MAC address table to file \"{task.host.name}_{timestamp}_2_MAC_table_before_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_mac_address_table"]), 
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_2_MAC_table_before_TrustSec_migration.pydict")
    
    task.run(task=data.echo_data,
             name=f"Save TrustSec server list to file \"{task.host.name}_{timestamp}_2_cts_server_list_before_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_cts_server_list"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_2_cts_server_list_before_TrustSec_migration.pydict")
    
    task.run(task=data.echo_data,
             name=f"Save TrustSec environment data to file \"{task.host.name}_{timestamp}_2_get_cts_environment_data_before_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_cts_environment_data"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_2_get_cts_environment_data_before_TrustSec_migration.pydict")

    task.run(task=data.echo_data,
              name=f"Save TrustSec role-based sgt-map to file \"{task.host.name}_{timestamp}_2_cts_role_base_sgt_map_before_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_cts_role_based_sgt_map_all"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_2_cts_role_base_sgt_map_before_TrustSec_migration.pydict")

    task.run(task=data.echo_data,
             name=f"Save IP device tracking table to file \"{task.host.name}_{timestamp}_2_IP_device_tracking_table_before_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_ip_device_tracking_all"]),
                 filename=f"{task.host.name}_{timestamp}_2_IP_device_tracking_table_before_TrustSec_migration.pydict",
                 path="migrate_TrustSec_servers/")
    task.run(task=data.echo_data,
             name=f"Save auth sessions to file \"{task.host.name}_{timestamp}_2_auth_sessions_before_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_auth_sessions"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_2_auth_sessions_before_TrustSec_migration.pydict")

    # save existing PAC keys to host inventory to use it later for validation
    task.host["pac_keys_before_migration"] = result.result["get_cts_pacs"]
    # save session table to host inventory to use it later for clear auth sessions
    task.host["auth_sessions_before_migration"] = result.result["get_auth_sessions"]
    get_task_progress_bar.update()


    # validate current configuration
        # reformat data if needed to be use with validate
    exist_servers_to_vld = []
    for server in existing_TrustSec_servers:
        exist_servers_to_vld.append({"IP_addr": server["IP_addr"], "state": "ALIVE"})
    exist_radius_servers_to_vld = {}
    for server in existing_TrustSec_servers:
        exist_radius_servers_to_vld[server["IP_addr"]] = {"STATE": "UP"}

    data_to_validate = [
    {
        "get_cts_server_list": {
            "_mode": "strict",
            "list": exist_servers_to_vld
        }
    },
    {
        "get_aaa_servers": exist_radius_servers_to_vld
    },
    {
        "get_radius_groups": {
            "radius_group_list": {
                "list": [existing_radius_server_group]
            }
        }
    }]

    tqdm.write(f"{Fore.GREEN}{task.host}: validating existing TrustSec servers and RADIUS group...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=data_to_validate,
                          name="Validate existing TrustSec servers and group",
                          severity_level=validate_task_logging_level)
    validate_task_progress_bar.update()

    # stop the task if validated data is not compliant
        # if provided servers are down or not exist
    if vld_result.result["get_cts_server_list"]["complies"] == False:
        dead_server_list = []
        for server in vld_result.result["get_cts_server_list"]["present"]:
                dead_server_list.append(server["IP_addr"])
        if dead_server_list:
            result = task.run(task=data.echo_data,
                name = f"Provided TrustSec servers: {str(dead_server_list).strip('[]')} are dead",
                message= "Please fix the existing TrustSec servers first before running the migration script again",
                severity_level=logging.ERROR)
            result[0].failed = True
            tqdm.write(f"{Fore.RED}{task.host}: provided TrustSec servers: {str(dead_server_list).strip('[]')} are dead. Task aborted! Check logs for more details.")
        if vld_result.result["get_cts_server_list"]["missing"]:
            result=task.run(task=data.echo_data,
                name = f"Provided TrustSec servers: {str(vld_result.result['get_cts_server_list']['missing']).strip('[]')} are missing",
                message= "Please make sure provided IP for the exisitng servers are correct or check if servers are in \"show cts server-list\" command output",
                severity_level=logging.ERROR)
            result[0].failed = True
            tqdm.write(f"{Fore.RED}{task.host}: validation for new TrustSec servers failed. Task aborted! Check logs for more details.")
        task.run(task=data.echo_data,
                name = "ABORTING TASK!",
                severity_level=logging.INFO)
        tqdm.write(f"{Fore.BLUE}{task.host}: TrustSec migration script finished")
        # exist task
        return
    
    if vld_result.result["get_aaa_servers"]["complies"] == False:
        dead_server_list = []
        for server, info in vld_result.result["get_aaa_servers"]["present"].items():
            if info["complies"] == False:
                dead_server_list.append(server)
        if dead_server_list:
            result = task.run(task=data.echo_data,
                        name = f"Provided radius servers: {str(dead_server_list).strip('[]')} are dead",
                        message= "Please fix the existing RADIUS servers first before running the migration task again",
                        severity_level=logging.ERROR)
            result[0].failed = True
            tqdm.write(f"{Fore.RED}{task.host}: provided RADIUS servers: {str(dead_server_list).strip('[]')} are down. Task aborted! Check logs for more info.")
        if vld_result.result["get_aaa_servers"]["missing"]:
            result=task.run(task=data.echo_data,
                        name = f"Provided radius servers: {str(vld_result.result['get_aaa_servers']['missing']).strip('[]')} are not configured",
                        message= "Please make sure provided details for the exisitng RADIUS servers are correct",
                        severity_level=logging.ERROR)
            result[0].failed = True
            tqdm.write(f"{Fore.RED}{task.host}: provided RADIUS servers: {str(vld_result.result['get_aaa_servers']['missing']).strip('[]')} not exist on the switch. Task aborted! Check logs for more info.")
        task.run(task=data.echo_data,
                 name = "ABORTING TASK!",
                 severity_level=logging.INFO)
        tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS server migration script finished")
        # exit task
        return

    # existing TrustSec servers are compliant
    task.run(task=data.echo_data, name = f"Provided existing TrustSec servers: {existing_TrustSec_servers} are UP")

    # if provided RADIUS server group not exists
    if vld_result.result["get_radius_groups"]["complies"] == False:
        result=task.run(task=data.echo_data,
            name = f"RADIUS group {existing_radius_server_group} not exists.",
            message= "Please make sure you provided correct RADIUS group name",
            severity_level=logging.ERROR)
        result[0].failed = True
        tqdm.write(f"{Fore.RED}{task.host}: {existing_radius_server_group} RADIUS group not exists. Task aborted! Check logs for more details.")
        task.run(task=data.echo_data, name = "ABORTING TASK!",
                 severity_level=logging.INFO)
        tqdm.write(f"{Fore.BLUE}{task.host}: RADIUS server migration script finished")
        # exit task
        return
    
    # existing RADIUS group is compliant
    task.run(task=data.echo_data,
             name=f"Provided radius group \"{existing_radius_server_group}\" exists")


    # build configuration using tempalates
    task.host["radius_servers"] = new_TrustSec_servers
    task.host["CoA_servers"] = CoA_servers
    task.host["test_user"] = test_user
    result = task.run(task=text.template_file,
            name="Build \"add TrutSec servers\" configuration",
            template="migrate_TrustSec_server.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)         

    # save built configuration to host inventory and to file
    task.host["TrustSec_server_config"] = result.result
    config_file = _create_tmp_file(result.result)
    tqdm.write(f"{task.host}: \"add TrustSec RADIUS servers\" configuration built")
    template_task_progress_bar.update()

    # deploy built configuration
    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add new TrustSec servers\" configuration...")
    try:
        task.run(task=networking.napalm_configure,
                name="Loading Configuration on the device",
                replace=False,
                #configuration=task.host["TrustSec_server_config"], # work only with SCP config transfer mode
                filename=config_file, # works both with SCP and Inline config transfer mode
                severity_level=config_task_logging_level)
    except NornirSubTaskError:
        tqdm.write(f"{Fore.RED}{task.host}: error during configuration deployment, automatic rollback attempted. Check provieded data and see logs for mode details.")
        tqdm.write(f"{Fore.BLUE}{task.host}: TrustSec server migration script finished")
        return
    config_task_progress_bar.update()


    # validate if new servers are up by authenticating test user
    tqdm.write(f"{Fore.GREEN}{task.host}: validating new TrustSec servers by authenticating test user...")    
    # it is performed only when task is run with dry_run=False as new servers
    # have to be deployed first
    if task.nornir.data.dry_run==False:
        result = task.run(task=networking.napalm_get,
                          getters=["get_test_radius_user_auth2"],
                          getters_options={"get_test_radius_user_auth2": { 
                          "user": test_user, "password": test_user_password,
                          "servers": new_TrustSec_servers}},
                          severity_level=validate_task_logging_level,
                          name="Validate test user auth for new RADIUS servers")
        for server, auth_result in result.result["get_test_radius_user_auth2"].items():
            if server != "username":
                if auth_result["authenticated"] == False:
                    comment = f"Failed user \"{test_user}\" authentication for new TrustSec server \"{auth_result['radius_server']}\", IP address \"{auth_result['radius_server_IP']}\"\
                        \nReason: {auth_result['reason']}"
                    echo_task = task.run(task=data.echo_data,
                                    name="Validate RADIUS test user authentication failed",
                                    message = comment,
                                    severity_level=logging.ERROR)
                    echo_task[0].failed = True
                    task.run(task=data.echo_data, name = "ABORTING TASK!")
                    tqdm.write(f"{Fore.RED}{task.host}: user authentication failed. Check provided data and see logs for more details.")
                    # rollback configuration
                    task.run(task=rollback, name="Rollback")
                    tqdm.write(f"{Fore.BLUE}{task.host}: TrustSec server migration script finished")
                    validate_task_progress_bar.update()
                    # exit task
                    return
                else:
                    task.run(task=data.echo_data,
                            name=f"Test user \"{test_user}\" successfully authenticated on \"{auth_result['radius_server']}\", IP address \"{auth_result['radius_server_IP']}\"")
    else:
        task.run(task=data.echo_data,
                 name = "Validate test user auth for new TrustSec servers")
    validate_task_progress_bar.update()


    # validate if PAC keys for new servers have been added
    """ if migrating to new Cisco ISE servers that have been deployed from copy 
    of the old ISE servers config the PAC key will be the same therefore can't validate
    tqdm.write(f"{Fore.GREEN}{task.host}: validating new PAC keys......") 
    if task.nornir.data.dry_run==False:
        data_to_validate = [
        {
            "get_cts_pacs": 
            {
                "_mode": "strict",
                "list": task.host["pac_keys_before_migration"]
            }
        }]

        vld_result = task.run(task=networking.napalm_validate,
                            validation_source=data_to_validate,
                            name=f"Validate if PAC keys for the new servers have been added",
                            severity_level=validate_task_logging_level)

        #if PAC key hasn't been added abort the task and rollback
        if vld_result.result["get_cts_pacs"]["extra"] == []:
            result = task.run(task=data.echo_data,
                    name = "PAC keys for new servers haven't been added successfully",
                    message= "Please make sure switch can reach TrustSec servers to issue the PAC key or run \"show cts provisioning\" to see outstanding jobs",
                    severity_level=logging.ERROR)
            result[0].failed = True
            tqdm.write(f"{Fore.RED}{task.host}: PAC keys for the new servers haven't been added successfully. Task aborted! Check logs for more details.")
            task.run(task=data.echo_data, name = "ABORTING TASK!")
            task.run(task=rollback, name="Rollback")
            tqdm.write(f"{Fore.BLUE}{task.host}: TrustSec servers migration script finished")
            # exit task
            return
        task.run(task=data.echo_data,
                 name = "PAC keys for new servers have been added")
    else:
        task.run(task=data.echo_data,
                 name = "Validate if PAC keys for new servers have been added")
    validate_task_progress_bar.update()
    """


    # clear auth seessions to have a fresh auth session table
    interface_to_be_auth_cleared_list = []
    for session in task.host["auth_sessions_before_migration"]:
        interface_to_be_auth_cleared_list.append(session["INTERFACE"])
    task.run(task=clear_auth_sessions,
             interface_list=interface_to_be_auth_cleared_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Clear authentication sessions before migration")    

    # wait to allow endpoints reauthenticate
    clear_auth_timeout = 2
    tqdm.write(f"{task.host}: waiting {clear_auth_timeout}m to allow endpoints to reauthenticate...")
    if task.nornir.data.dry_run==False:
        for i in range(clear_auth_timeout):
            time.sleep(60)
            task.run(task=keep_alive,
                    severity_level=logging.INFO,
                    name="Keep connection up till endpoints reauthenticate") 
            if i+1 != clear_auth_timeout:
                tqdm.write(f"{task.host}: waiting {clear_auth_timeout}m to allow endpoints to reauthenticate - {clear_auth_timeout-i-1} minute(s) left")


    # get fresh auth sessions table and save to file and to host inventory
    result = task.run(task=get_auth_sessions,
                      tqdm_message="getting fresh auth sessions table and save to file...",
                      task_progress_bar=get_task_progress_bar,
                      task_logging_level=get_task_logging_level,
                      name=f"Get fresh auth sessions table and save to file \"{task.host.name}_{timestamp}_3_auth_sessions_after_1st_clear.pydict\"")
    _create_file(config=str(result[1].result["get_auth_sessions"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_3_auth_sessions_after_1st_clear.pydict")
    # save auth sessions table to host inventory to use it for validation after servers migration
    task.host["auth_sessions_after_1st_clear"] = result[1].result["get_auth_sessions"]

    # get fresh auth sessions details and save to file and to host inventory
    tqdm.write(f"{task.host}: getting auth dot1x and mab sessions details and save to files...")
    result = task.run(task=networking.napalm_get,
                      getters=["get_auth_sessions_dot1x_details",
                               "get_auth_sessions_mab_details"],
                      severity_level=get_task_logging_level,
                      name=f"Get fresh auth dot1x sessions  and save to file \"{task.host.name}_{timestamp}_3_auth_sessions_dot1x_details_after_1st_clear.pydict\"")
    _create_file(config=str(result.result["get_auth_sessions_dot1x_details"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_3_auth_sessions_dot1x_details_after_1st_clear.pydict")
    _create_file(config=str(result.result["get_auth_sessions_mab_details"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_3_auth_sessions_mab_details_after_1st_clear.pydict")
    get_task_progress_bar.update()

    # save auth  sessions details to host inventory to use it for validation after servers migration
    task.host["auth_dot1x_sessions_details_after_1st_clear"] = result.result["get_auth_sessions_dot1x_details"]
    task.host["auth_mab_sessions_details_after_1st_clear"] = result.result["get_auth_sessions_mab_details"]



    # add new TrustSec servers to RADIUS group (above old servers)
    # build configuration using tempalates to add new and remove old servers
    task.host["radius_server_group"] = existing_radius_server_group
    task.host["old_radius_servers"] = existing_TrustSec_servers         
    result = task.run(task=text.template_file,
                      name="Build \"add TrustSec servers to RADIUS group (and remove old servers)\" configuration",
                      template="migrate_radius_server_add_new_servers_to_group.j2",
                      path=f"configuration_templates/{task.host.platform}",
                      severity_level=template_task_logging_level)         

    # save built configuration to host inventory and to file
    task.host["TrustSec_server_config"] = result.result
    config_file = _create_tmp_file(result.result)
    tqdm.write(f"{task.host}: \"add new TrustSec servers to RADIUS group (and remove old servers)\" configuration built")
    template_task_progress_bar.update()

    # deploy built configuration
    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add new TrustSec servers to RADIUS group (and remove old servers)\" configuration...")
    task.run(task=networking.napalm_configure,
            name="Loading Configuration on the device",
            replace=False,
            #configuration=task.host["TrustSec_server_config"], # work only with SCP config transfer mode
            filename=config_file, # works both with SCP and Inline config transfer mode
            severity_level=config_task_logging_level)
    config_task_progress_bar.update()

    # add old TrustSec servers back to RADIUS group (below new servers) for global failover scenario
    # build configuration using tempalates          
    result = task.run(task=text.template_file,
            name="Build \"add old TrustSec servers to RADIUS group (below new servers)\" configuration",
            template="migrate_radius_server_add_old_servers_to_group.j2",
            path=f"configuration_templates/{task.host.platform}",
            severity_level=template_task_logging_level)         

    # save built configuration to host inventory and to file
    task.host["TrustSec_server_config"] = result.result
    config_file = _create_tmp_file(result.result)
    tqdm.write(f"{task.host}: \"add old TrustSec servers to RADIUS group (below new servers)\" configuration built")
    template_task_progress_bar.update()

    # deploy built configuration
    tqdm.write(f"{Fore.YELLOW}{task.host}: deploying \"add old TrustSec servers to RADIUS group (below new servers)\" configuration...")
    task.run(task=networking.napalm_configure,
            name="Loading Configuration on the device",
            replace=False,
            #configuration=task.host["TrustSec_server_config"], # work only with SCP config transfer mode
            filename=config_file, # works both with SCP and Inline config transfer mode
            severity_level=config_task_logging_level)
    config_task_progress_bar.update()


    # clear TrustSec environment data to connect to new servers
    task.run(task=clear_TrustSec_env_data,
             name="Clear TrustSec environment data",
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             config_task_logging_level=config_task_logging_level)

    # validate TrustSec state 
    data_to_validate = [{
        "get_cts_environment_data_state": {
            "current_state": "COMPLETE",
            "last_state": "Failed"
        }
    }]
    tqdm.write(f"{Fore.GREEN}{task.host}: validating TrustSec state for new servers...")
    if task.nornir.data.dry_run==False:        
        state = "pending"
        while state == "pending":
            time.sleep(30) # default timeout
            validation_result = task.run(task=networking.napalm_validate,
                                    name="Validate TrustSec state for new servers",
                                    validation_source=data_to_validate,
                                    severity_level=validate_task_logging_level)
            if validation_result.result["get_cts_environment_data_state"]["present"]["current_state"]["complies"] == True:
                state="COMPLETED"
                tqdm.write(f"{Fore.GREEN}{task.host}: TrustSec state for new servers is \"COMPLETE\"")
            if validation_result.result["get_cts_environment_data_state"]["present"]["last_state"]["complies"] == True:
                result=task.run(task=data.echo_data,
                                name = "TrustSec for new servers failed",
                                message= "Please login to the switch to troubleshoot further",
                                severity_level=logging.ERROR)
                state="Failed"
                result[0].failed = True
                tqdm.write(f"{Fore.RED}{task.host}: TrustSec state for new servers is \"Failed\". Check logs for more details.")
            if state == "pending":
                time.sleep(30)
                tqdm.write(f"{Fore.GREEN}{task.host}: still validating...")
    else:
        task.run(task=data.echo_data,
                 name = "Validate TrustSec state for new servers")
    validate_task_progress_bar.update()

    # validate if new TrustSec servers are up
    new_TrustSec_servers_list = []
    for server in new_TrustSec_servers:
        new_TrustSec_servers_list.append({"IP_addr": server["IP_addr"], "state": "ALIVE"})
        

    data_to_validate = [
    {
        "get_cts_server_list": 
        {
            "list": new_TrustSec_servers_list
        }
    }]
    if task.nornir.data.dry_run==False:
        cts_server_validation_result = task.run(task=validate,
            name=f"Validate if new TrustSec servers are up",
            data_to_validate=data_to_validate,
            tqdm_message="validating if new TrustSec servers are up...",
            task_progress_bar=validate_task_progress_bar,
            task_logging_level=validate_task_logging_level)

        if cts_server_validation_result[1].result["get_cts_server_list"]["complies"] == False:
            dead_server_list = []
            for server in cts_server_validation_result[1].result["get_cts_server_list"]["present"]:
                dead_server_list.append(server["IP_addr"])
            if dead_server_list:
                result = task.run(task=data.echo_data,
                    name = f"New TrustSec servers: {str(dead_server_list).strip('[]')} are dead",
                    message= "Log in to the switch to troubleshoot further",
                    severity_level=logging.ERROR)
                result[0].failed = True
                tqdm.write(f"{Fore.RED}{task.host}: new TrustSec servers: {str(dead_server_list).strip('[]')} are dead!")
            if cts_server_validation_result[1].result["get_cts_server_list"]["missing"]:
                result=task.run(task=data.echo_data,
                    name = f"TrustSec not established to servers: {str(cts_server_validation_result[1].result['get_cts_server_list']['missing']).strip('[]')}",
                    message= "Log in to the switch to troubleshoot further",
                    severity_level=logging.ERROR)
                result[0].failed = True 
                tqdm.write(f"{Fore.RED}{task.host}: TrustSec not established to servers: {str(cts_server_validation_result[1].result['get_cts_server_list']['missing']).strip('[]')}")
        else:
            # provided TrustSec servers are up
            task.run(task=data.echo_data, 
                     name = f"New TrustSec servers: {new_TrustSec_servers_list} are UP")
    else:
        task.run(task=data.echo_data,
                 name = "Validate if new TrustSec servers are up")
        tqdm.write(f"{Fore.GREEN}{task.host}: validating if new TrustSec servers are up...")
        validate_task_progress_bar.update()

    time.sleep(1)
    # build list of interfaces to be auth cleared after migration
    # 802.1x auth interfaces are excluded as they need to be bounced to force Anyconnect restarts posture checking
    interface_to_be_auth_cleared_list = []
    for session in task.host["auth_sessions_after_1st_clear"]:
        if session["METHOD"] != "dot1x":
            interface_to_be_auth_cleared_list.append(session["INTERFACE"])
    task.run(task=clear_auth_sessions,
             interface_list=interface_to_be_auth_cleared_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Clear MAB authentication sessions after migration")   

    # bounce 802.1x ports
    # build list of interface to be bounced  
    dot1x_interface_to_be_bounced_list = []
    for session in task.host["auth_sessions_after_1st_clear"]:
        if session["METHOD"] == "dot1x":
            dot1x_interface_to_be_bounced_list.append(session["INTERFACE"])
    # delete duplicate interfaces - multiple endpoints on one interface
    dot1x_interface_to_be_bounced_list = list(set(dot1x_interface_to_be_bounced_list))
    
    task.host["dot1x_interface_to_be_bounced_list"] = dot1x_interface_to_be_bounced_list

    tqdm.write(f"{task.host}: bouncing interfaces with 802.1x auth sessions")
    task.run(task=bounce_interface,
             interface_list=dot1x_interface_to_be_bounced_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Bounce 802.1x interfaces to restart Anyconnect posture")  

    # wait to allow endpoints authenticate to new servers
    port_bounce_auth_timeout = 5
    tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout}m to allow endpoints to authenticate to new RADIUS/TrustSec servers...")
    if task.nornir.data.dry_run==False:
        for i in range(port_bounce_auth_timeout):
            time.sleep(60)
            task.run(task=keep_alive,
                    severity_level=logging.INFO,
                    name="Keep connection up till endpoints reauthenticate") 
            if i+1 != port_bounce_auth_timeout:
                tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout}m to allow endpoints to authenticate to new RADIUS/TrustSec servers - {port_bounce_auth_timeout-i-1} minute(s) left")


    # get auth sessions table and save to file and to host inventory
    result = task.run(task=get_auth_sessions,
                      tqdm_message="getting auth sessions table (after migration) and save to file...",
                      task_progress_bar=get_task_progress_bar,
                      task_logging_level=get_task_logging_level,
                      name=f"Get auth sessions table and save to file \"{task.host.name}_{timestamp}_4_auth_sessions_after_TrustSec_migration.pydict\"")
    _create_file(config=str(result[1].result["get_auth_sessions"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_4_auth_sessions_after_TrustSec_migration.pydict")
    # save auth sessions table to host inventory to use it for validation after servers migration
    task.host["auth_sessions_after_migration"] = result[1].result["get_auth_sessions"]


    # comper sessions before and after migration
    for session in task.host["auth_sessions_after_1st_clear"]:
        del session["SESSION"]
    sessions_before_migration = [
        {
            "get_auth_sessions": {
                "_mode": "strict",
                "list": list(task.host["auth_sessions_after_1st_clear"])
                }
        }
    ]

    tqdm.write(f"{Fore.GREEN}{task.host}: validating authentication sessions...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=sessions_before_migration,
                          name="Validate for any missing endpoints after clear auth",
                          severity_level=validate_session_task_logging_level)
    validate_task_progress_bar.update()
    if validate_session_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Validate for any missing endpoints after clear auth")

    # bounce ports with missing endpoints (silent/no reauth host issue)
    # build list of interface to be bounced  
    interface_to_be_bounced_list = []
    for session in vld_result.result["get_auth_sessions"]["missing"]:
        interface_to_be_bounced_list.append(session["INTERFACE"])
    # delete duplicate interfaces - multiple endpoints on one interface
    interface_to_be_bounced_list = list(set(interface_to_be_bounced_list))
    
    task.host["interfaces_to_be_bounced"] = interface_to_be_bounced_list

    tqdm.write(f"{task.host}: bouncing interfaces with missing auth sessions")
    task.run(task=bounce_interface,
             interface_list=interface_to_be_bounced_list,
             template_task_progress_bar=template_task_progress_bar,
             config_task_progress_bar=config_task_progress_bar,
             template_task_logging_level=template_task_logging_level,
             config_task_logging_level=config_task_logging_level,
             name="Bounce interfaces that have missing auth endpoints")

    # wait 5 minute to allow missing endpoints to authenticate to new servers
    port_bounce_auth_timeout = 5
    tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout}m to allow missing endpoints to authenticate...")
    if (interface_to_be_bounced_list and (task.nornir.data.dry_run==False)):
        for i in range(port_bounce_auth_timeout):
            time.sleep(60)
            task.run(task=keep_alive,
                    severity_level=logging.INFO,
                    name="Keep connection up till endpoints reauthenticate") 
            if i+1 != port_bounce_auth_timeout:
                tqdm.write(f"{task.host}: waiting {port_bounce_auth_timeout}m to allow missing endpoints to authenticate - {port_bounce_auth_timeout-i-1} minute(s) left")


    # get auth sessions after auth cleared and ports bounced and save to file
    result = task.run(task=get_auth_sessions,
                      tqdm_message="getting auth sessions table after bouncing ports and save to file...",
                      task_progress_bar=get_task_progress_bar,
                      task_logging_level=get_task_logging_level,
                      name=f"Get auth sessions table and save to file \"{task.host.name}_{timestamp}_5_auth_sessions_after_port_bounce.pydict\"")
    _create_file(config=str(result[1].result["get_auth_sessions"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_5_auth_sessions_after_port_bounce.pydict")
    # save auth sessions table to host inventory to use it wiht validate for missing endpoints
    task.host["auth_sessions_after_port_bounce"] = result[1].result["get_auth_sessions"]

    # get missing session after migration
    tqdm.write(f"{Fore.GREEN}{task.host}: validating missing authentication sessions and save to file...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=sessions_before_migration,
                          name=f"Validate missing auth endpoints after port bounce and save to \"{task.host.name}_{timestamp}_6_missing_auth_endpoints_after_TrustSec_migration.pydict\"",
                          severity_level=validate_session_task_logging_level)
    validate_task_progress_bar.update()
    if validate_session_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Validate missing auth endpoints after port bounce and save to \"{task.host.name}_{timestamp}_6_missing_auth_endpoints_after_TrustSec_migration.pydict\"")
    _create_file(config=str(vld_result.result["get_auth_sessions"]["missing"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_6_missing_auth_endpoints_after_TrustSec_migration.pydict")

    # get auth sessions details that change the autorization after migration
    for key in task.host["auth_dot1x_sessions_details_after_1st_clear"]:
            # delete unique data
            del task.host["auth_dot1x_sessions_details_after_1st_clear"][key]["SESSION"]
            del task.host["auth_dot1x_sessions_details_after_1st_clear"][key]["ACS_ACL"]
            del task.host["auth_dot1x_sessions_details_after_1st_clear"][key]["URL_REDIRECT"]
    for key in task.host["auth_mab_sessions_details_after_1st_clear"]:
            del task.host["auth_mab_sessions_details_after_1st_clear"][key]["SESSION"]
            del task.host["auth_mab_sessions_details_after_1st_clear"][key]["ACS_ACL"]
            del task.host["auth_mab_sessions_details_after_1st_clear"][key]["URL_REDIRECT"]
    sessions_details_before_migration = [
        {
            "get_auth_sessions_dot1x_details": task.host["auth_dot1x_sessions_details_after_1st_clear"],
            "get_auth_sessions_mab_details": task.host["auth_mab_sessions_details_after_1st_clear"]

        }
    ]
    tqdm.write(f"{Fore.GREEN}{task.host}: validating authentication session details and save to file...")    
    vld_result = task.run(task=networking.napalm_validate,
                          validation_source=sessions_details_before_migration,
                          name=f"Validate auth session details and save to \"{task.host.name}_{timestamp}_6_auth_sessions_details_after_migration.pydict\"",
                          severity_level=validate_session_task_logging_level)
    validate_task_progress_bar.update()
    if validate_session_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Validate auth session details and save to \"{task.host.name}_{timestamp}_6_auth_sessions_details_after_migration.pydict\"")

    _create_file(config=str(vld_result.result),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_6_auth_sessions_details_after_migration.pydict")


    # get information and save to files 
    tqdm.write(f"{task.host}: getting MAC address table, TrustSec data, IP device tracking table and save to files...")    
    result = task.run(task=networking.napalm_get,
                      getters=["get_mac_address_table",
                               "get_cts_server_list",
                               "get_cts_environment_data",
                               "get_cts_role_based_sgt_map_all",
                               "get_ip_device_tracking_all"],
                      severity_level=get_task_logging_level,
                      name="Get MAC address table, TrustSec data and save to files")
    if get_task_logging_level == logging.DEBUG:
        task.run(task=data.echo_data,
             name="Get MAC address table, TrustSec data, IP device tracking table and save to files")
    
    task.run(task=data.echo_data,
             name=f"Save MAC address table to file \"{task.host.name}_{timestamp}_7_MAC_table_after_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_mac_address_table"]), 
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_7_MAC_table_after_TrustSec_migration.pydict")
    
    task.run(task=data.echo_data,
             name=f"Save TrustSec server list to file \"{task.host.name}_{timestamp}_7_cts_server_list_after_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_cts_server_list"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_7_cts_server_list_after_TrustSec_migration.pydict")
    
    task.run(task=data.echo_data,
             name=f"Save TrustSec environment data to file \"{task.host.name}_{timestamp}_7_get_cts_environment_data_after_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_cts_environment_data"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_7_get_cts_environment_data_agter_TrustSec_migration.pydict")

    task.run(task=data.echo_data,
             name=f"Save TrustSec role-based sgt-map to file \"{task.host.name}_{timestamp}_7_cts_role_base_sgt_map_after_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_cts_role_based_sgt_map_all"]),
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_7_cts_role_base_sgt_map_after_TrustSec_migration.pydict")

    task.run(task=data.echo_data,
             name=f"Save IP device tracking table to file \"{task.host.name}_{timestamp}_7_IP_device_tracking_table_after_TrustSec_migration.pydict\"")
    _create_file(config=str(result.result["get_ip_device_tracking_all"]),
                 filename=f"{task.host.name}_{timestamp}_7_IP_device_tracking_table_after_migration.pydict", 
                 path="migrate_TrustSec_servers/")
    get_task_progress_bar.update()   

    # get running config and save to file
    task.run(task=get_config, dest_config_name="config_after_TrustSec_migration",
             task_progress_bar=get_task_progress_bar,
             task_logging_level=logging.NOTSET,
             tqdm_message="getting configuration and save to file...",
             name=f"Get run config and save to file \"{task.host.name}_{timestamp}_8_running_cofnig_after_TrustSec_migration.conf\"")
    _create_file(config=task.host["config_after_TrustSec_migration"]["running"], 
                 path="migrate_TrustSec_servers/",
                 filename=f"{task.host.name}_{timestamp}_8_running_config_after_TrustSec_migration.conf")

    tqdm.write(f"{Fore.BLUE}{task.host}: TrustSec migration script finished")
