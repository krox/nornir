from nornir import InitNornir
import NornirTasks
from nornir.core.inventory import ConnectionOptions
from nornir.plugins.functions.text import print_result
import logging
from nornir.core.deserializer.inventory import Inventory
from pprint import pprint
from nornir.plugins.tasks import networking
from nornir.core.filter import F
import getpass
from tqdm import tqdm
from colorama import Fore
import importlib
import json
import sys
import re
import ast
from pathlib import Path
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter

def menu():
    """While loop menu to browse inventory and run tasks on filtered 
    hosts/groups"
    """

    def print_inventory_summary():
        """Function to display inventory summary"""
        print("\n", 20 * "-", "Imported Inventory Summary", 20 * "-") # header
        print(f"Number of groups: {len(nr.inventory.groups)}")
        print(f"Number of hosts: {len(nr.inventory.hosts)}")

    def print_menu():
        """Function to display main menu"""
        print("\n", 30 * "-", "MENU", 30 * "-") # header
        print("1. Display imported hosts ")
        print("2. Display imported groups ")
        print("3. Gather information")
        print("4. Configuration tasks")
        print("5. Other tasks")
        print("6. Validate data")
        print("7. Help")
        print("8. Exit")
        print(67 * "-")

    def print_hosts_menu():
        """Function to display "hosts" menu"""
        print("\n", 29 * "-", "Hosts", 29 * "-") # header
        print("1. Display all hosts ")
        print("2. Display hosts by group")
        print("3. Display filtered hosts")
        print("4. Help")
        print("5. Cancel")
        print(67 * "-")

    def print_get_menu():
        """Function to display available getters to run to collect 
        information
        """
        print("\n", 27 * "-", "Getters", 27 * "-") # header
        i = 1
        for getter in getters: # iterate through avaliable getters
            print(f"{i}. {getter}")
            i = i+1
        print(f"{i}. Help")
        print(f"{i+1}. Cancel")
        print(67 * "-")

    def print_config_menu():
        """Function to display configuration tasks menu"""
        if nr.inventory.defaults.connection_options["napalm"].extras["optional_args"]["inline_transfer"] == True:
            config_mode = "Inline Transfer (Cisco IOS only)"
        else:
            config_mode = "SCP"
        print("\n", 25 * "-", "Config tasks", 25 * "-") # header
        i = 1
        for task in config_tasks: # interate through available config tasks
            print(f"{i}. {task}")
            i = i+1
        print(f"\n{i}. Change config transfer mode")
        print(f"{i+1}. Help")
        print(f"{i+2}. Cancel")
        print(f"Config transfer mode enabled: {config_mode}")
        print(67 * "-")

    def print_other_tasks_menu():
        """Function to display oter tasks menu"""
        print("\n", 25 * "-", "Other tasks", 25 * "-") # header
        i = 1
        for task in other_tasks: # interate through available tasks
            print(f"{i}. {task}")
            i = i+1
        print(f"\n{i}. Help")
        print(f"{i+1}. Cancel")
        print(67 * "-")

    def print_validate_menu():
        """Display validate task menu"""
        print("\n", 25 * "-", "Other tasks", 25 * "-") # header
        print("1. Validate")
        print("2. Help")
        print("3. Cancel")
        print(67 * "-")     

    def print_selection_menu(task_type):
        """Function to display host selection menu to specify where to run the
        task
        """
        print("\n", 25 * "-", "Select where to run the task", 25 * "-")
        print("1. All hosts ")
        print("2. On hosts in specific group")
        print("3. On filtered hosts")
        print("4. Help")
        print("5. Cancel")
        if task_type == "config":        
            print("6. Enable/disable dry_run mode")
            print(f"dry_run mode enabled: {nr.data.dry_run}")
        print(67 * "-")

    def print_config_transfer_mode_menu():
        """Function to display and select config transfer mode
        """
        if nr.inventory.defaults.connection_options["napalm"].extras["optional_args"]["inline_transfer"] == True:
            config_mode = "Inline Transfer (1)"
        else:
            config_mode = "SCP (2)"
        print("\n", 25 * "-", "Select config transfer mode", 25 * "-")
        print("1. Inline Transfer mode - Cisco IOS only")
        print("2. SCP (Secure Copy) mode - suggested")   
        print("3. Help")
        print("4. Go back")
        print(f"Config transfer mode enabled: {config_mode}")
        print(67 * "-")

    def print_selection_menu_help(print_dry_run_mode=False):
        """Display print_selection_menu help"""
        # display help
        print("\nSpecify where to run the task you selected")
        print("\n1. All hosts - run the task on all hosts in inventory")
        print("2. On hosts in specific group - run the task on all hosts in specified group")
        print("3. On filtered hosts - run the task on hosts that match the specified filter e.g. location=Berlin or platfrom=junos")
        if print_dry_run_mode==True:
            print("\ndry_run mode - if enabled/True no configuration will be applied to devices. Useful to test the change and see what would happen if the change was run in dry_run disabled/False.")
            print("You still need to have admin rights as NAPALM creates candiate_config, merge_config and rollback_config files to allow doing this.")
        input("\nEnter any key to continue..")

    def print_config_transfer_mode_help():
        print("\nSpecify to either use SCP (Secury Copy) or Inline transfer to upload changes to devices.")
        print("\nSCP is suggested as it works on all platforms but requires to have SCP server enabled on device.")
        print("On Cisco IOS \"aaa authorization exec\" is also requied to start an exec (shell)")
        print("Check https://napalm.readthedocs.io/en/latest/support/index.html#general-support-matrix too see all platform requirements and caveats.")
        print("\nInline transfer works only on Cisco IOS and doesn't requrire any additional configuration.")
        print("It only supports clear text files therefore can't be used generally with config_replace as for example banners contain encoded ETX character(ASCII 3) that looks like \"^C\" in the config.")
        print("More here https://napalm.readthedocs.io/en/latest/support/ios.html")                   
        input("\nEnter any key to continue..")

    # import NornirTasks.py module to use NornirTasks
    NornirTasks_module = importlib.import_module("NornirTasks")

    # get available getters from NornirTasks module
    getters = [task for task in dir(NornirTasks_module) if task.startswith("get_")]
    # list of configuration tasks available in NornirTasks
    config_tasks = [
        "add_radius_dynamic_auth", "add_radius_server", 
        "add_radius_server_to_group", "bounce_interface", 
        "disable_TrustSec_enforcement", "enable_TrustSec_enforcement", 
        "migrate_radius_servers", "migrate_TrustSec_servers", 
        "remove_radius_dynamic_auth", "remove_radius_server", 
        "remove_radius_server_from_group", "replace_config",
    ]
    
    other_tasks = [
        "clear_auth_sessions", "delete_files", 
        "test_radius_user_auth"
    ]

    # create a list of host attributes to use for prompt autocompletion
    attribute_list = ["name", "hostname", "platform",]
    for host in nr.inventory.hosts:
        for attribute in nr.inventory.hosts[host].data:
            attribute_list.append(attribute)
    attribute_list = list(set(attribute_list))
    filter_completer = WordCompleter(attribute_list)


    # define global attributes including task progress bars 
    get_task_num: int
    template_task_num: int
    config_task_num: int
    validate_task_num: int
    task_data : dict
    debug_logs = bool

    def request_data(task):
        """Wrapper for config_task and other_task to request data from user 
        required to run tasks, it also assigns correct number of subtasks 
        needed for tqdm task progress bars
        """
        nonlocal get_task_num, template_task_num, config_task_num
        nonlocal validate_task_num, task_data, debug_logs
        # assign default number of each subtasks:
        get_task_num = 0
        template_task_num = 1
        config_task_num = 1
        validate_task_num = 0
        task_data = {}
        debug_logs = False # allow user selects logs level for logs to display
        if task == "add_radius_dynamic_auth":
            # ask user to provide data
            task_data = { "radius_servers": req_data_add_radius_dynamic_auth()}
        elif task == "add_radius_server":
            task_data = { "radius_servers": req_data_add_radius_server()}
        elif task == "add_radius_server_to_group":
            task_data = req_data_add_radius_server_to_group()
        elif task == "bounce_interface":
            template_task_num = 2
            config_task_num = 2
            task_data = { "interface_list": req_data_bounce_interface()}
        elif task == "disable_TrustSec_enforcement":
            template_task_num = 0
        elif task == "enable_TrustSec_enforcement":
            template_task_num = 0
        elif task == "migrate_radius_servers":
            debug_logs = True
            template_task_num = 9
            config_task_num = 9
            get_task_num = 8
            validate_task_num = 5
            task_data = req_data_migrate_radius_servers()
        elif task == "migrate_TrustSec_servers":
            debug_logs = True
            template_task_num = 10
            config_task_num = 10
            get_task_num = 7
            validate_task_num = 6
            task_data = req_data_migrate_TrustSec_servers()
        elif task == "replace_config":
            template_task_num = 0
            task_data = req_data_replace_config()
        elif task == "remove_radius_dynamic_auth":
            task_data = req_data_remove_radius_dynamic_auth()
        elif task == "remove_radius_server":
            task_data = req_data_remove_radius_server()
        elif task == "remove_radius_server_from_group":
            task_data = req_data_remove_radius_server_from_group()
        elif task == "clear_auth_sessions":
            task_data = req_data_clear_auth_sessions()
        elif task == "delete_files":
            task_data = req_data_delete_files()
        elif task == "test_radius_user_auth":
            config_task_num = 0
            validate_task_num = 1
            task_data = req_data_test_radius_user_auth()
        elif task == "validate":
            task_data = req_data_validate()
        else:
            raise(Exception(f"{task} is not added to request_data(config_task) wrapper"))

    def req_data_add_radius_dynamic_auth():
        """Request data from user required to run 
        NornirTasks.add_radius_dynamit_auth task
        """
        num_of_servers = int(input(f"Provide number of servers you would like to configure: "))
        servers = []
        for i in range(num_of_servers):
            print(f"Configuring server number {i+1}:")
            server_IP = input(f"Provide server IP address: ")    
            server_key =  input(f"Provide server key: ")     
            servers.append({"IP_addr": server_IP, "key": server_key})
        return servers

    def req_data_add_radius_server():
        """Request data from user required to run 
        NornirTasks.add_radius_server task
        """
        num_of_servers = int(input(f"Provide number of servers you would like to configure: "))
        servers = []
        for i in range(num_of_servers):
            print(f"Configuring server number {i+1}:")
            server_name = input(f"Provide server name: ")    
            server_IP =  input(f"Provide server IP address: ")
            server_auth_port = input(f"Provide port number used for RADIUS authentication: ")    
            server_acct_port =  input(f"Provide port number used for RADIUS accounting: ")
            server_key = input(f"Provide server key: ")    
            server_tester_username = input(f"Provide automate-tester username to use for server probing: ")
            server_tester_password = input(f"Provide automate_tester password: ")    
            servers.append({
                "server_name": server_name, "IP_addr": server_IP,
                "auth_port": server_auth_port, "acct_port": server_acct_port,
                "key": server_key, "tester_username": server_tester_username,
                "tester_password": server_tester_password})
        return servers

    def req_data_add_radius_server_to_group():
        """Request data from user required to run 
        NornirTasks.add_radius_server_to_group task
        """
        radius_server_group = input("Provide RADIUS group name: ")
        num_of_servers = int(input(f"Provide number of servers you would like to add to \"{radius_server_group}\" RADIUS group: "))
        servers = []
        for i in range(num_of_servers):
            print(f"Configuring server number \"{i+1}\":")
            server_name = input(f"Provide server name: ")    
            server_IP =  input(f"Provide server IP address (required if host is legacy Cisco IOS): ")
            server_auth_port = input(f"Provide port number used for RADIUS authentication (required if host is legacy Cisco IOS): ")    
            server_acct_port =  input(f"Provide port number used for RADIUS accounting (required if host is legacy Cisco IOS): ")   
            servers.append({
                "server_name": server_name, "IP_addr": server_IP,
                "auth_port": server_auth_port, "acct_port": server_acct_port})
        return {"radius_server_group": radius_server_group, 
                "radius_servers": servers}       

    def req_data_bounce_interface():
        """Request data from user required to run NornirTasks.bounce_interface
         task
        """
        print("\nProvide name of interfaces to be bounced (separated by \", \"), for example Gi1/1, Te1/5, GigabitEthernet8/8, eth1/3")
        interface_list = (input()).split(", ")
        return interface_list
    
    def req_data_migrate_radius_servers():
        try:
            file_to_open = Path("config_task_data/migrate_radius_servers.json")
            with open(file_to_open, 'r') as f:
                data = json.load(f)
            existing_radius_servers = data["existing_radius_servers"]
            existing_radius_server_group = data["existing_radius_server_group"]
            new_radius_servers = data["new_radius_servers"]
            test_user = data["test_user"]
            test_user_password = data["test_user_password"]
            CoA_servers = data["CoA_servers"]
        except json.JSONDecodeError:
            print("\nError occured duing decoding JSON")
            print(f"Make sure you provided correct data in \"{file_to_open}\" file and select the task again.")
            print(f"You can help yourself with \"{file_to_open}_fileformat.txt\" file.")
            sys.exit(0)
        except FileNotFoundError:
            print(f"Please make sure \"{file_to_open}\" exists and select the task again.")
            sys.exit(0)
        except KeyError as e:
            print(f"\nLooks like \"{file_to_open}\" is missing required data: {str(e)}.")
            print("Correct the file and select the task again.")
            sys.exit(0)
        else:
            print()
            pprint(data)
            print("\nThis task will run with the above data.")
            print(f"To change the data update \"{file_to_open}\" file and select the task again.")
            input("\nEnter any key to continue..")
            return {"existing_radius_servers": existing_radius_servers, 
                    "existing_radius_server_group": existing_radius_server_group, 
                    "new_radius_servers": new_radius_servers,
                    "test_user": test_user,
                    "test_user_password": test_user_password,
                    "CoA_servers": CoA_servers}

    def req_data_migrate_TrustSec_servers():
        try:
            file_to_open = Path("config_task_data/migrate_TrustSec_servers.json")
            with open(file_to_open, 'r') as f:
                data = json.load(f)
            existing_TrustSec_servers = data["existing_TrustSec_servers"]
            existing_radius_server_group = data["existing_radius_server_group"]
            new_TrustSec_servers = data["new_TrustSec_servers"]
            test_user = data["test_user"]
            test_user_password = data["test_user_password"]
            CoA_servers = data["CoA_servers"]
        except json.JSONDecodeError:
            print("\nError occured duing decoding JSON")
            print(f"Make sure you provided correct data in \"{file_to_open}\" file and select the task again")
            print(f"You can help yourself with \"{file_to_open}_fileformat.txt\" file.")
            sys.exit(0)
        except FileNotFoundError:
            print(f"Please make sure \"{file_to_open}\" file exists and select the task again.")
            sys.exit(0)
        except KeyError as e:
            print(f"\nLooks like \"{file_to_open}\" is missing required data: {str(e)}.")
            print("Correct the file and select the task again.")
            sys.exit(0)
        else:
            print()
            pprint(data)
            print("\nThis task will run with the above data.")
            print(f"To change the data update \"{file_to_open}\" file and select the task again.")
            input("\nEnter any key to continue..")
            return {"existing_TrustSec_servers": existing_TrustSec_servers, 
                    "existing_radius_server_group": existing_radius_server_group, 
                    "new_TrustSec_servers": new_TrustSec_servers,
                    "test_user": test_user,
                    "test_user_password": test_user_password,
                    "CoA_servers": CoA_servers}

    def req_data_remove_radius_dynamic_auth():
        """Request data from user required to run 
        NornirTasks.remove_radius_dynamit_auth task
        """
        num_of_servers = int(input(f"Provide number of servers you would like to remove: "))
        servers = []
        for i in range(num_of_servers):
            print(f"Server number {i+1}:")
            server_IP = input(f"Provide server IP address: ")  
            servers.append({"IP_addr": server_IP})
        return {"radius_servers": servers}

    def req_data_remove_radius_server():
        """Request data from user required to run 
        NornirTasks.remove_radius_server task
        """
        num_of_servers = int(input(f"Provide number of servers you would like to remove: "))
        servers = []
        for i in range(num_of_servers):
            print(f"Server number {i+1}:")
            server_name = input(f"Provide server name: ")
            server_IP =  input(f"Provide server IP address (required if host is legacy Cisco IOS): ") 
            servers.append({"server_name": server_name, "IP_addr": server_IP})
        return {"radius_servers": servers}            

    def req_data_remove_radius_server_from_group():
        """Request data from user required to run 
        NornirTasks.remove_radius_server_from_group task 
        """
        radius_server_group = input("Provide RADIUS group name: ")
        num_of_servers = int(input(f"Provide number of servers you would like to remove from \"{radius_server_group}\" RADIUS group: "))
        servers = []
        for i in range(num_of_servers):
            print(f"Server number \"{i+1}\":")
            server_name = input(f"Provide server name: ")    
            server_IP =  input(f"Provide server IP address (required if host is legacy Cisco IOS): ")
            servers.append({
                "server_name": server_name, "IP_addr": server_IP})
        return {"radius_server_group": radius_server_group, 
                "radius_servers": servers}

    def req_data_replace_config():
        print("\nCopy configuration you want to use to \"config_to_replace.txt\" file. The task will implement only difference between the file and device running config.")
        try:
            print("Use get_config first to save the config to the file and make required changes.")
            with open("config_to_replace.txt", 'r') as f:
                data = f.read()
        except FileNotFoundError:
            print("Please make sure \"config_to_replace.txt\" file exists and select the task again.")
            print("Use get_config first to save the config to the file and make required changes.")
            sys.exit(0)
        else:
            input("\nEnter any key to continue..")
            return {"config": data, "config_type": "running"}

    def req_data_clear_auth_sessions():
        """Request data from user required to run 
        NornirTasks.clear_auth_sessions task
        """
        print("\nProvide name of interfaces to be authentication session (802.1x/MAB) cleard (separated by \", \"), for example Gi1/1, Te1/5, GigabitEthernet8/8, eth1/3")
        interface_list = (input()).split(", ")
        return {"interface_list": interface_list}

    def req_data_delete_files():
        """Request data from user required to run NornirTasks.delete_files 
        task
        """
        file_list = (input("Provide name of files to be deleted (separated by \", \"): ")).split(", ")
        return {"file_list": file_list}

    def req_data_test_radius_user_auth():
        """Request data from user required to run 
        NornirTasks.test_radius_user_auth task 
        """
        username = input("Provide user name: ")
        password = input("Provide user password: ")
        server_name = input("Provide RADIUS server name: ")    
        server_IP =  input("Provide server IP address (required if host is legacy Cisco IOS): ")
        return {"user": username, "password": password,
                "radius_server_name": server_name,
                "radius_server_ip": server_IP}          

    def req_data_validate():
        try:
            file_to_open = Path("validate_data/validate_data.pydict")
            with open(file_to_open, 'r') as f:
                raw_data = f.read()
                data = [ast.literal_eval(raw_data)]
        except FileNotFoundError:
            print(f"Please make sure \"{file_to_open}\" exists and select the task again.")
            sys.exit(0)
        except SyntaxError as e:
            if e.msg == "unexpected EOF while parsing":
                print(f"\nLooks like the \"{file_to_open}\" file is not ending properly, make sure dictionary is closed properly or you have no blank lines and select the task again.")
                sys.exit(0)
            elif e.msg == "unexpected indent":
                print(f"\nLooks like the \"{file_to_open}\" is corrupted, make sure the file format is correct or compare with \"validate_data_example.pydict\" and select the task again.")
                sys.exit(0)
            else:
                print(e.msg)
                print(f"\nLooks like the \"{file_to_open}\" is corrupted, make sure the file format is correct or compare with \"validate_data_example.pydict\" and select the task again.")
                sys.exit(0)
        else:
            print()
            pprint(data)
            print("\nThis task will run with the above data.")
            print(f"To change the data update \"{file_to_open}\" file and select the task again.")
            input("\nEnter any key to continue..")
            return {"data_to_validate": data}





    # main menu
    menu_loop = True
    while menu_loop: # main menu loop
        print_inventory_summary()
        print_menu()
        choice = input("Enter your choice [1-8]: ")

        if choice == "1": # Display imported hosts        
            hosts_menu_loop = True
            while hosts_menu_loop:
                print_hosts_menu()
                choice = input("Enter your choice [1-5]: ")

                if choice == "1": # Display all hosts
                    print(f"\n Number of all hosts: {len(nr.inventory.hosts)}\n")
                    pprint(nr.inventory.hosts, indent=10)
                
                elif choice == "2": # Display hosts by group
                    group_name = input("Provide group name: ")
                    print_group_members(group_name)

                elif choice == "3": # Display filtered hosts
                    filter_name = prompt(
                        "By what would you like to filter hosts (use Tab for help): ",
                        completer=filter_completer)# e.g. location
                    filter_value = input("Provide value: ") # e.g. london
                    if filter_name == "port":
                        # to allow to compare with port(int) in inventory
                        filter_value = int(filter_value)
                    print_filtered_hosts(filter_name, filter_value)
                
                elif choice == "4":
                    # display help
                    print("\nDisplay hosts that have been imported to the inventory")
                    print("\n1. \"Display all hosts\" lists all devices that have been imported to the inventory")
                    print("2. \"Display host by group\" lists all devices in specified group")
                    print("3. \"Display filtered hosts\" lists all devices that match specified filter e.g. location=Berlin or device_type=access")
                    input("\nEnter any key to continue..")
                elif choice == "5":
                    # cancel
                    hosts_menu_loop = False
                else:
                    input("Wrong menu selection. Enter any key to try again..")
        elif choice == "2":# Display imported groups
            print(f"\n  Imported groups: {len(nr.inventory.groups)}\n")
            pprint(nr.inventory.groups, indent=10)
        elif choice == "3": # Gather information
            get_menu_loop = True
            while get_menu_loop:
                print_get_menu() # list available getters
                # number of available getters + help + cancel
                getter_choice = input(
                    f"Enter your choice [1-{len(getters)+2}]: ")
                # catch an error if provided value is not a digit
                try:
                    int(getter_choice)
                except ValueError:
                    input("Wrong menu selection. Enter any key to try again..")
                    break
                # verify if getter chose
                if (int(getter_choice) >= 1 and
                        int(getter_choice) <= len(getters)):
                    selection_menu_loop = True
                    while selection_menu_loop: # Select where to run the task
                        print(f"{Fore.BLUE}\nSelected task: {getters[int(getter_choice)-1]}")
                        print_selection_menu(task_type="get")
                        choice = input("Enter your choice [1-5]: ")
                        if choice == "1": # All hosts
                            # create filtered inventory to be used later
                            filtered_nr = nr
                            print()
                            pprint(filtered_nr.inventory.hosts)
                            print(f"\n {len(filtered_nr.inventory.hosts)} hosts in inventory ")
                        elif choice == "2": # display hosts filtered by group
                            group_name = input("Provide group name: ")
                            # if no hosts found
                            if print_group_members(group_name) == False:
                                choice = "Do nothing"
                            # created filtered invenotry
                            filtered_nr = nr.filter(filter_func=lambda 
                                h: group_name in h.groups)
                        elif choice == "3": # run on filtered hosts
                            filter_name = prompt(
                                "By what would you like to filter hosts (use Tab for help): ",
                                completer=filter_completer)
                            filter_value = input("Provide value: ")
                            if filter_name == "port":
                                # to allow to compare with port(int) in inventory
                                filter_value = int(filter_value)
                            # create filtered inventory
                            filtered_nr = print_filtered_hosts(filter_name, 
                                                               filter_value)
                            if filtered_nr == False: # if no hosts found
                                choice = "Do nothing"
                        elif choice == "4":
                            # display help
                            print_selection_menu_help()
                        elif choice == "5":
                            # cancel
                            selection_menu_loop = False
                        elif choice == "Do nothing":
                            pass
                        else:
                            input("Wrong menu selection. Enter any key to try again..")
                        # run the task on selected hosts
                        if (choice == "1" or choice == "2" or choice == "3"):
                            print("\n  Running task on above devices...")                
                            confirmation_prompt_loop = True
                            while confirmation_prompt_loop:
                                task_run_confirmation = input("\nConfirm [Y/N]: ")
                                if (task_run_confirmation == "Y" or 
                                        task_run_confirmation == "y"):
                                    confirmation_prompt_loop = False
                                    go_back = False
                                elif (task_run_confirmation == "N" or
                                        task_run_confirmation == "n"):
                                    print(f"\n Going back to hosts selection menu")
                                    confirmation_prompt_loop = False
                                    go_back = True
                                else:
                                    input("Wrong selection. Enter any key to try again..")
                            if go_back == False:
                                for host in filtered_nr.inventory.hosts:
                                    # if connection is not already established 
                                    if filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        provide_creds()
                                        break
                                print()
                                # create task progress bar
                                get_bar = tqdm(total=len(filtered_nr.inventory.hosts), 
                                            desc=getters[int(getter_choice)-1])
                                # get chose getter from NornirTasks module 
                                getter = getattr(NornirTasks_module, 
                                                getters[int(getter_choice)-1])
                                # run chose getter with a task progress bar
                                if getters[int(getter_choice)-1] == "get_config":
                                    result = filtered_nr.run(task=getter,
                                        task_progress_bar=get_bar,
                                        task_logging_level=logging.DEBUG)
                                else:
                                    result = filtered_nr.run(
                                        task=getter, task_progress_bar=get_bar)
                                if getters[int(getter_choice)-1] == "get_auth_session_number":
                                    all_device_MAB_session_number = 0
                                    all_device_dot1x_session_number = 0
                                    all_device_unknown_session_number = 0
                                    all_device_total_session_number = 0
                                    for device in result:
                                        # only devices for which the task run with no issue
                                        if result[device][1].exception == None:
                                            all_device_total_session_number += result[device][1].result["get_auth_session_number"]["all_sessions"]
                                            all_device_MAB_session_number += result[device][1].result["get_auth_session_number"]["MAB_sessions"]
                                            all_device_dot1x_session_number += result[device][1].result["get_auth_session_number"]["dot1x_sessions"]
                                            all_device_unknown_session_number += result[device][1].result["get_auth_session_number"]["unknown_sessions"]                   
                                tqdm.write("Task finished") # write to task progress console 
                                get_bar.close()
                                print_result(result)
                                if getters[int(getter_choice)-1] == "get_auth_session_number":
                                    print()
                                    print(f"Number of all sessions on all devices {all_device_total_session_number}")
                                    print(f"Number of dot1x sessions on all devices {all_device_dot1x_session_number}")
                                    print(f"Number of MAB sessions on all devices {all_device_MAB_session_number}")
                                    print(f"Number of unknown sessions on all devices {all_device_unknown_session_number}") 
                                if getters[int(getter_choice)-1] == "get_config":
                                    get_config_loop = True
                                    while get_config_loop:
                                        config_type = input("\nType which config you want to display [running|startup|candidate]: ")
                                        if bool(re.match('run', config_type, re.IGNORECASE)) == True:
                                            for device in result:
                                                print(f"\n{device} running config:\n")
                                                print(result[device][1].result["config"]["running"])
                                            get_config_loop = False
                                        elif bool(re.match('start', config_type, re.IGNORECASE)) == True:
                                            for device in result:
                                                print(f"\n{device} startup config:\n")
                                                print(result[device][1].result["config"]["startup"])
                                            get_config_loop = False
                                        elif bool(re.match('cand', config_type, re.IGNORECASE)) == True:
                                            for device in result:
                                                print(f"\n{device} candidate config:\n")
                                                if result[device][1].result["config"]["candidate"] == "":
                                                    print("Candidate config not supported on this device")
                                                print(result[device][1].result["config"]["candidate"])
                                            get_config_loop = False
                                        else:
                                            input("Wrong selection. Enter any key to try again..")
                                # prevent using already established connections for failed hosts
                                for host in filtered_nr.data.failed_hosts:
                                    if not filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        del filtered_nr.inventory.hosts[host].connections["napalm"]
                                # Reset failed hosts if any as Nornir won't let 
                                # run more tasks on them if the last task failed
                                filtered_nr.data.reset_failed_hosts()
                                another_task_loop = True
                                while another_task_loop:
                                    another_task_confirmation = input("\nTask finished. Would you like to run it again? [Y/N]: ")
                                    if (another_task_confirmation == "Y" or 
                                            another_task_confirmation == "y"):
                                        another_task_loop = False
                                    elif (another_task_confirmation == "N" or
                                            another_task_confirmation == "n"):
                                        print(f"\n Going back to main menu")
                                        another_task_loop = False
                                        selection_menu_loop = False
                                        get_menu_loop = False
                                    else:
                                        input("Wrong selection. Enter any key to try again..")
                elif int(getter_choice) == len(getters)+1:
                    # display help
                    print("\nAvailable getters to run and supported platforms. Any getter can be extended to work on more platforms and community platforms can be added or own drivers can be created if needed - ask Damo.")
                    print("\nget_aaa_servers : IOS")
                    print("get_arp_table : EOS, IOS, NXOS_SSH")
                    print("get_auth_sessions : IOS")
                    print("get_bgp_config : EOS, IOS, IOSXR, JUNOS")
                    print("get_bgp_neighbors : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_bgp_neighbors_detail : EOS, IOS, IOSXR, JUNOS")
                    print("get_config : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_environment : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_facts : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_flash : IOS")
                    print("get_interfaces : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_interfaces_counters : EOS, IOS, IOSXR, JUNOS")
                    print("get_interfaces_ip : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_ip_device_tracking_table : IOS")
                    print("get_ipv6_neighbors_table : IOS, JUNOS")
                    print("get_lldp_neighbors : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_lldp_neighbors_detail : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_mac_address_table : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_network_instances : EOS, IOS, JUNOS, NXOS")
                    print("get_ntp_peers : IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_ntp_servers : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_ntp_stats : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_optics : EOS, IOS, JUNOS")
                    print("get_probes_config : IOS, IOSXR, JUNOS")
                    print("get_probes_results : IOSXR, JUNOS")
                    print("get_radius_groups : IOS")
                    print("get_snmp_infomration : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("get_trustsec_PAC_keys : IOS")
                    print("get_trustsec_env : IOS")
                    print("get_trustsec_servers : IOS")
                    print("get_trustsec_sgt_map : IOS")
                    print("get_users : EOS, IOS, IOSXR, JUNOS, NXOS, NXOS_SSH")
                    print("\nCheck https://napalm.readthedocs.io/en/latest/support/index.html#general-support-matrix for platform requirements and caveats.")
                    input("\nEnter any key to continue..")
                elif int(getter_choice) == len(getters)+2:
                    # cancel
                    get_menu_loop = False
                else:
                    input("Wrong menu selection. Enter any key to try again..")
        elif choice == "4": # configuration tasks
            config_menu_loop = True
            while config_menu_loop:
                print_config_menu()
                config_task_choice = input(
                    f"Enter your choice [1-{len(config_tasks)+3}]: ")
                # catch error if provided value is not a digit
                try:
                    int(config_task_choice)
                except ValueError:
                    input("Wrong menu selection. Enter any key to try again..")
                    break
                # if config task selected
                if (int(config_task_choice) >= 1 and 
                        int(config_task_choice) <= len(config_tasks)):
                    # call wrapper to request task data from user and to 
                    # assign correct number of task progress bars
                    request_data(config_tasks[int(config_task_choice)-1])
                    selection_menu_loop = True
                    dry_mode_menu = 3
                    while selection_menu_loop: # select where to run it
                        while dry_mode_menu:
                            dry_mode_enabled = input("\nWould you like to run this task in dry_run mode (no changes will be made on hosts) [Y/N]: ")
                            if dry_mode_enabled == "Y" or dry_mode_enabled == "y":
                                nr.data.dry_run = True
                                dry_mode_menu = False
                            elif dry_mode_enabled == "N" or dry_mode_enabled == "n":
                                nr.data.dry_run = False
                                dry_mode_menu = False
                            else:
                                input("Wrong menu selection. Enter any key to try again...")
                        print(f"{Fore.BLUE}\nSelected task: {config_tasks[int(config_task_choice)-1]}")
                        print_selection_menu(task_type="config")
                        choice = input("Enter your choice [1-6]: ")
                        if choice == "1": # all hosts
                            # create filtered inventory to be used later
                            filtered_nr = nr
                            print()
                            pprint(filtered_nr.inventory.hosts)
                            print(f"\n {len(filtered_nr.inventory.hosts)} hosts in inventory ")
                        elif choice == "2":# hosts in group
                            group_name = input("Provide group name: ")
                            # if no hosts found
                            if print_group_members(group_name) == False:
                                choice = "Do nothing"
                            else:
                                # created filtered invenotry
                                filtered_nr = nr.filter(filter_func=lambda 
                                    h: group_name in h.groups)
                        elif choice == "3":# run on filtered hosts
                            filter_name = prompt(
                                "By what would you like to filter hosts (use Tab for help): ",
                                completer=filter_completer)
                            filter_value = input("Provide value: ")
                            if filter_name == "port":
                                # to allow to compare with port(int) in inventory
                                # catch error if provided value is not a digit
                                try:
                                    int(config_task_choice)
                                except ValueError:
                                    input("Wrong menu selection. Enter any key to try again..")
                                    choice = "Do nothing"
                                filter_value = int(filter_value)
                            # create filtered inventory
                            filtered_nr = print_filtered_hosts(filter_name, 
                                                               filter_value)
                            if filtered_nr == False: # if no hosts found
                                choice = "Do nothing"
                        elif choice == "4":
                            # print help
                            print_selection_menu_help(print_dry_run_mode=True)
                        elif choice == "5":
                            # cancel
                            selection_menu_loop = False
                        elif choice == "6":
                            dry_mode_menu2 = True
                            while dry_mode_menu2:
                                dry_mode_enabled = input("\nWould you like to run the task in dry_run mode (no configuration will be applied on hosts) [Y/N]: ")
                                if dry_mode_enabled == "Y" or dry_mode_enabled == "y":
                                    nr.data.dry_run = True
                                    dry_mode_menu2 = False
                                elif dry_mode_enabled == "N" or dry_mode_enabled == "n":
                                    nr.data.dry_run = False
                                    dry_mode_menu2 = False
                                else:
                                    input("Wrong menu selection. Enter any key to try again...")
                        elif choice == "Do nothing":
                            pass
                        else:
                            input("Wrong menu selection. Enter any key to try again..")
                        if (choice == "1" or choice == "2" or choice == "3"):
                            print("\n  Running task on above devices...")
                            confirmation_prompt_loop = True
                            while confirmation_prompt_loop:
                                task_run_confirmation = input("\nConfirm [Y/N]: ")
                                if (task_run_confirmation == "Y" or 
                                        task_run_confirmation == "y"):
                                    confirmation_prompt_loop = False
                                    go_back = False
                                elif (task_run_confirmation == "N" or
                                        task_run_confirmation == "n"):
                                    print(f"\n Going back to hosts selection menu")
                                    confirmation_prompt_loop = False
                                    go_back = True
                                else:
                                    input("Wrong selection. Enter any key to try again..")
                            if go_back == False:
                                for host in filtered_nr.inventory.hosts:
                                    # if connection is not already established 
                                    if filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        provide_creds()
                                        break
                                # create task progress bars
                                print()
                                progress_bars = {}
                                if get_task_num != 0:
                                    get_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*get_task_num,
                                        desc="getting information")
                                    progress_bars["get_task_progress_bar"] = get_task_progress_bar
                                if template_task_num != 0:
                                    template_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*template_task_num,
                                        desc="building configuration")
                                    progress_bars["template_task_progress_bar"] = template_task_progress_bar
                                if config_task_num != 0:
                                    config_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*config_task_num,
                                        desc="deploying configuration")
                                    progress_bars["config_task_progress_bar"] = config_task_progress_bar
                                if validate_task_num != 0:
                                    validate_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*validate_task_num,
                                        desc="validating information and testing")
                                    progress_bars["validate_task_progress_bar"] = validate_task_progress_bar
                                # get chose config task from NornirTasks module 
                                config_task = getattr(NornirTasks_module,
                                    config_tasks[int(config_task_choice)-1])
                                # run chose task with task progress bars and user
                                # provided data
                                result = filtered_nr.run(task=config_task,
                                    **task_data, **progress_bars)
                                if get_task_num != 0:
                                    get_task_progress_bar.close()
                                if template_task_num != 0:
                                    template_task_progress_bar.close()
                                if config_task_num != 0:
                                    config_task_progress_bar.close()
                                if validate_task_num != 0:
                                    validate_task_progress_bar.close()
                                logs_display_prompt_loop = True
                                logs_display_counter = 0
                                while logs_display_prompt_loop:
                                    if logs_display_counter == 0:
                                        display_logs = input("\nDisplay task logs? [Y/N]: ")
                                    elif logs_display_counter > 0:
                                        display_logs = input("\nDisplay task logs again? [Y/N]: ")
                                    if display_logs == "Y" or display_logs == "y":
                                        logs_display_counter +=1
                                        print()
                                        pprint(filtered_nr.inventory.hosts, indent=10)
                                        selected_device = input("\nProvide full name of the device or type \"all\": ")
                                        if debug_logs == True:
                                            debug_prompt_loop = True
                                            while debug_prompt_loop:
                                                display_debug = input("\nDisplay getters and validate task content? [Y/N]: ")
                                                if display_debug == "Y" or display_debug == "y":
                                                    print()
                                                    if selected_device.lower() == "all":
                                                        print_result(result, severity_level=logging.DEBUG)
                                                    else:   
                                                        try:
                                                            print_result(result[str(selected_device).lower()], severity_level=logging.DEBUG)
                                                        except KeyError as e:
                                                            print(f"Couldn't find the device: {e}. Please try again.")
                                                    debug_prompt_loop = False
                                                elif display_debug == "N" or display_debug == "n":
                                                    print()
                                                    if selected_device.lower() == "all":
                                                        print_result(result)
                                                    else:
                                                        try:
                                                            print_result(result[str(selected_device).lower()])
                                                        except KeyError as e:
                                                            print(f"Couldn't find the device: {e}. Please try again.")
                                                    debug_prompt_loop = False
                                                else:
                                                    input("Wrong selection. Enter any key to try again...")
                                        else:
                                            if selected_device.lower() == "all":
                                                print_result(result)
                                            else:
                                                print_result(result[str(selected_device).lower()])
                                            logs_display_prompt_loop = False
                                    elif display_logs == "n" or display_logs == "N":
                                        logs_display_prompt_loop = False
                                    else:
                                        input("Wrong selection. Enter any key to try again...")
                                # prevent using already established connections for failed hosts
                                for host in filtered_nr.data.failed_hosts:
                                    if not filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        del filtered_nr.inventory.hosts[host].connections["napalm"]
                                # Reset failed hosts if any as Nornir won't let 
                                # run more tasks on them if the last task failed
                                filtered_nr.data.reset_failed_hosts()
                                another_task_loop = True
                                while another_task_loop:
                                    another_task_confirmation = input("\nTask finished. Would you like to run it again? [Y/N]: ")
                                    if (another_task_confirmation == "Y" or 
                                            another_task_confirmation == "y"):
                                        another_task_loop = False
                                    elif (another_task_confirmation == "N" or
                                            another_task_confirmation == "n"):
                                        print(f"\n Going back to main menu")
                                        another_task_loop = False
                                        selection_menu_loop = False
                                        config_menu_loop = False
                                    else:
                                        input("Wrong selection. Enter any key to try again..")
                elif int(config_task_choice) == len(config_tasks)+1:
                    config_transfer_mode_menu_loop = True
                    while config_transfer_mode_menu_loop: # select config mode
                        print_config_transfer_mode_menu()
                        choice = input("Enter your choice [1-4]: ")
                        if choice == "1":
                            nr.inventory.defaults.connection_options["napalm"].extras["optional_args"]["inline_transfer"] = True
                        elif choice == "2":
                            nr.inventory.defaults.connection_options["napalm"].extras["optional_args"]["inline_transfer"] = False
                        elif choice == "3":
                            # display config transfer mode help
                            print_config_transfer_mode_help()
                        elif choice == "4":
                            #cancel
                            config_transfer_mode_menu_loop = False
                        else:
                            input("Wrong menu selection. Enter any key to try again..")
                elif int(config_task_choice) == len(config_tasks)+2:
                    # display help
                    print("\nAvailable config tasks to run and supported platforms. Any task can be extended to work on more platforms and community platforms can be added or own drivers can be created if needed - ask Damo.")
                    print("Config tasks will create canditate_config.txt, merge_config.txt and rollback_config.txt files on device's flash to allow for automatic rollback, atomic changes (all commands succeeded or none will be applied) and configuration merge to implement only lines that are not yet in the device running config.")
                    print("\nadd_radius_dynamic_auth : IOS")
                    print("add_radius_server : IOS")
                    print("add_radius_server_to_group : IOS")
                    print("bounce_interface : IOS")
                    print("disable_TrustSec_enforcement : IOS")
                    print("enable_TrustSec_enforcement : IOS")
                    print("migrate_radius_servers : IOS")
                    print("migrate_TrustSec_servers : IOS")
                    print("remove_radius_dynamic_auth : IOS")
                    print("remove_radius_server : IOS")
                    print("remove_radius_server_from_group : IOS")
                    print("replace_config : IOS")
                    print("\n\"Change config transfer mode\" - select to use SCP or Inline transfer") 
                    print("\nCheck https://napalm.readthedocs.io/en/latest/support/index.html#general-support-matrix for platform requirements and caveats.")
                    input("\nEnter any key to continue..")
                elif int(config_task_choice) == len(config_tasks)+3:
                    # cancel
                    config_menu_loop = False
                else:
                    input("Wrong menu selection. Enter any key to try again..")
        elif choice == "5": # other tasks
            other_task_menu_loop = True
            while other_task_menu_loop:
                print_other_tasks_menu()
                other_task_choice = input(
                    f"Enter your choice [1-{len(other_tasks)+2}]: ")
                # catch error if provided value is not a digit
                try:
                    int(other_task_choice)
                except ValueError:
                    input("Wrong menu selection. Enter any key to try again..")
                    break
                # if task selected
                if (int(other_task_choice) >= 1 and 
                        int(other_task_choice) <= len(other_tasks)):
                    # call wrapper to request task data from user and to 
                    # assign correct number of task progress bars
                    request_data(other_tasks[int(other_task_choice)-1])
                    selection_menu_loop = True
                    dry_mode_menu = True
                    while selection_menu_loop: # select where to run it
                        while dry_mode_menu:
                            dry_mode_enabled = input("\nWould you like to run the task in dry_run mode (no configuration will be applied on hosts) [Y/N]: ")
                            if dry_mode_enabled == "Y" or dry_mode_enabled == "y":
                                nr.data.dry_run = True
                                dry_mode_menu = False
                            elif dry_mode_enabled == "N"or dry_mode_enabled == "n":
                                nr.data.dry_run = False
                                dry_mode_menu = False
                            else:
                                input("Wrong menu selection. Enter any key to try again...")
                        print(f"{Fore.BLUE}\nSelected task: {other_tasks[int(other_task_choice)-1]}")
                        print_selection_menu(task_type="config")
                        choice = input("Enter your choice [1-6]: ")
                        if choice == "1": # all hosts
                            # create filtered inventory to be used later
                            filtered_nr = nr
                            print()
                            pprint(filtered_nr.inventory.hosts)
                            print(f"\n {len(filtered_nr.inventory.hosts)} hosts in inventory ")
                        elif choice == "2":# hosts in group
                            group_name = input("Provide group name: ")
                            # if no hosts found
                            if print_group_members(group_name) == False:
                                choice = "Do nothing"
                            else:
                                # created filtered invenotry
                                filtered_nr = nr.filter(filter_func=lambda 
                                    h: group_name in h.groups)
                        elif choice == "3":# run on filtered hosts
                            filter_name = prompt(
                                "By what would you like to filter hosts (use Tab for help): ",
                                completer=filter_completer)
                            filter_value = input("Provide value: ")
                            if filter_name == "port":
                                # to allow to compare with port(int) in inventory
                                # catch error if provided value is not a digit
                                try:
                                    filter_value = int(filter_value)
                                except ValueError:
                                    input("Please provide correct port number. Enter any key to try again..")
                                    choice = "Do nothing"
                            # create filtered inventory
                            filtered_nr = print_filtered_hosts(filter_name, 
                                                               filter_value)
                            if filtered_nr == False: # if no hosts found
                                choice = "Do nothing"
                        elif choice == "4":
                            # print device selection menu help
                            print_selection_menu_help(print_dry_run_mode=True)
                        elif choice == "5":
                            # cancel
                            selection_menu_loop = False
                        elif choice == "6":
                            dry_mode_menu2 = True
                            while dry_mode_menu2:
                                dry_mode_enabled = input("\nWould you like to run the task in dry_run mode (no configuration will be applied on hosts) [Y/N]: ")
                                if dry_mode_enabled == "Y" or dry_mode_enabled == "y":
                                    nr.data.dry_run = True
                                    dry_mode_menu2 = False
                                elif dry_mode_enabled == "N" or dry_mode_enabled == "n":
                                    nr.data.dry_run = False
                                    dry_mode_menu2 = False
                                else:
                                    input("Wrong menu selection. Enter any key to try again...")
                        elif choice == "Do nothing":
                            pass
                        else:
                            input("Wrong menu selection. Enter any key to try again..")
                        if (choice == "1" or choice == "2" or choice == "3"):
                            print("\n  Running task on above devices...")
                            confirmation_prompt_loop = True
                            while confirmation_prompt_loop:
                                task_run_confirmation = input("\nConfirm [Y/N]: ")
                                if (task_run_confirmation == "Y" or 
                                        task_run_confirmation == "y"):
                                    confirmation_prompt_loop = False
                                    go_back = False
                                elif (task_run_confirmation == "N" or
                                        task_run_confirmation == "n"):
                                    print(f"\n Going back to hosts selection menu")
                                    confirmation_prompt_loop  = False
                                    go_back = True
                                else:
                                    input("Wrong selection. Enter any key to try again..")
                            if go_back == False:
                                for host in filtered_nr.inventory.hosts:
                                    # if connection is not already established 
                                    if filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        provide_creds()
                                        break
                                # create task progress bars
                                progress_bars = {}
                                print()
                                if get_task_num != 0:
                                    get_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*get_task_num,
                                        desc="gathering information")
                                    progress_bars["get_task_progress_bar"] = get_task_progress_bar
                                if template_task_num != 0:
                                    template_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*template_task_num,
                                        desc="building configuration")
                                    progress_bars["template_task_progress_bar"] = template_task_progress_bar
                                if config_task_num != 0:
                                    config_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*config_task_num,
                                        desc="deploying configuration")
                                    progress_bars["config_task_progress_bar"] = config_task_progress_bar
                                if validate_task_num != 0:
                                    validate_task_progress_bar = tqdm(
                                        total=len(filtered_nr.inventory.hosts)*validate_task_num,
                                        desc="validating information and testing")
                                    progress_bars["validate_task_progress_bar"] = validate_task_progress_bar
                                # get chose config task from NornirTasks module 
                                other_task = getattr(NornirTasks_module,
                                    other_tasks[int(other_task_choice)-1])
                                # run chose task with task progress bars and user
                                # provided data
                                result = filtered_nr.run(task=other_task,
                                    **task_data, **progress_bars)
                                if get_task_num != 0:
                                    get_task_progress_bar.close()
                                if template_task_num != 0:
                                    template_task_progress_bar.close()
                                if config_task_num != 0:
                                    config_task_progress_bar.close()
                                if validate_task_num != 0:
                                    validate_task_progress_bar.close()
                                print_result(result)
                                # prevent using already established connections for failed hosts
                                for host in filtered_nr.data.failed_hosts:
                                    if not filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        del filtered_nr.inventory.hosts[host].connections["napalm"]
                                # Reset failed hosts if any as Nornir won't let 
                                # run more tasks on them if the last task failed
                                filtered_nr.data.reset_failed_hosts()
                                another_task_loop = True
                                while another_task_loop:
                                    another_task_confirmation = input("\nTask finished. Would you like to run it again? [Y/N]: ")
                                    if (another_task_confirmation == "Y" or 
                                            another_task_confirmation == "y"):
                                        another_task_loop = False
                                    elif (another_task_confirmation == "N" or
                                            another_task_confirmation == "n"):
                                        print(f"\n Going back to main menu")
                                        another_task_loop = False
                                        selection_menu_loop = False
                                        config_menu_loop = False
                                    else:
                                        input("Wrong selection. Enter any key to try again..")
                elif int(other_task_choice) == len(other_tasks)+1:
                    # print other task menu help
                    print("\nOther tasks that can't be categorise as config tasks or getters. Any task can be extended to work on more platforms and community platforms can be added or own drivers can be created if needed - ask Damo.")
                    print("\nclear_auth_sessions : IOS")
                    print("delete_files: IOS")
                    print("test_radius_user_auth: IOS")
                    input("\nEnter any key to continue..")
                    pass
                elif int(other_task_choice) == len(other_tasks)+2:
                    # cancel
                    other_task_menu_loop = False
                else:
                    input("Wrong menu selection. Enter any key to try again..")
        elif choice == "6": # validate data
            validate_menu_loop = True
            while validate_menu_loop:
                print_validate_menu()
                validate_task_choice = input(
                    f"Enter your choice [1-3]: ")
                # if task selected
                if (validate_task_choice == "1"):
                    # call wrapper to request task data from user and to 
                    # assign correct number of task progress bars
                    request_data(task="validate")
                    selection_menu_loop = True
                    while selection_menu_loop: # select where to run it
                        print_selection_menu(task_type="validate")
                        choice = input("Enter your choice [1-6]: ")
                        if choice == "1": # all hosts
                            # create filtered inventory to be used later
                            filtered_nr = nr
                            print()
                            pprint(filtered_nr.inventory.hosts)
                            print(f"\n {len(filtered_nr.inventory.hosts)} hosts in inventory ")
                        elif choice == "2":# hosts in group
                            group_name = input("Provide group name: ")
                            # if no hosts found
                            if print_group_members(group_name) == False:
                                choice = "Do nothing"
                            else:
                                # created filtered invenotry
                                filtered_nr = nr.filter(filter_func=lambda 
                                    h: group_name in h.groups)
                        elif choice == "3":# run on filtered hosts
                            filter_name = prompt(
                                "By what would you like to filter hosts (use Tab for help): ",
                                completer=filter_completer)
                            filter_value = input("Provide value: ")
                            if filter_name == "port":
                                # to allow to compare with port(int) in inventory
                                # catch error if provided value is not a digit
                                try:
                                    filter_value = int(filter_value)
                                except ValueError:
                                    input("Please provide correct port number. Enter any key to try again..")
                                    choice = "Do nothing"
                            # create filtered inventory
                            filtered_nr = print_filtered_hosts(filter_name, 
                                                               filter_value)
                            if filtered_nr == False: # if no hosts found
                                choice = "Do nothing"
                        elif choice == "4":
                            # print device selection menu help
                            print_selection_menu_help()
                        elif choice == "5":
                            # cancel
                            selection_menu_loop = False
                        elif choice == "Do nothing":
                            pass
                        else:
                            input("Wrong menu selection. Enter any key to try again..")
                        if (choice == "1" or choice == "2" or choice == "3"):
                            print("\n  Running task on above devices...")
                            confirmation_prompt_loop = True
                            while confirmation_prompt_loop:
                                task_run_confirmation = input("\nConfirm [Y/N]: ")
                                if (task_run_confirmation == "Y" or 
                                        task_run_confirmation == "y"):
                                    confirmation_prompt_loop = False
                                    go_back = False
                                elif (task_run_confirmation == "N" or
                                        task_run_confirmation == "n"):
                                    print(f"\n Going back to hosts selection menu")
                                    confirmation_prompt_loop  = False
                                    go_back = True
                                else:
                                    input("Wrong selection. Enter any key to try again..")
                            if go_back == False:
                                for host in filtered_nr.inventory.hosts:
                                    # if connection is not already established 
                                    if filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        provide_creds()
                                        break
                                # create task progress bars
                                progress_bars = {}
                                print()
                                task_progress_bar = tqdm(
                                    total=len(filtered_nr.inventory.hosts)*1,
                                    desc="validating information and testing")
                                progress_bars["task_progress_bar"] = task_progress_bar
                                # get chose config task from NornirTasks module 
                                validate_task = getattr(NornirTasks_module,"validate")
                                # run chose task with task progress bars and user
                                # provided data
                                result = filtered_nr.run(task=validate_task,
                                    **task_data, **progress_bars)
                                task_progress_bar.close()
                                print_result(result)
                                # prevent using already established connections for failed hosts
                                for host in filtered_nr.data.failed_hosts:
                                    if not filtered_nr.inventory.hosts[host].connections.get("napalm") == None:
                                        del filtered_nr.inventory.hosts[host].connections["napalm"]
                                # Reset failed hosts if any as Nornir won't let 
                                # run more tasks on them if the last task failed
                                filtered_nr.data.reset_failed_hosts()
                                another_task_loop = True
                                while another_task_loop:
                                    another_task_confirmation = input("\nTask finished. Would you like to run it again? [Y/N]: ")
                                    if (another_task_confirmation == "Y" or 
                                            another_task_confirmation == "y"):
                                        another_task_loop = False
                                    elif (another_task_confirmation == "N" or
                                            another_task_confirmation == "n"):
                                        print(f"\n Going back to main menu")
                                        another_task_loop = False
                                        selection_menu_loop = False
                                        config_menu_loop = False
                                    else:
                                        input("Wrong selection. Enter any key to try again..")
                elif validate_task_choice == "2":
                    # print validate task menu help
                    print("\nUse valdiate to check if your devices are compliant with provided data.")
                    print("Use getters to collect data first to see the data structure, upload it to \"validate_data.pydict\" file and edit it to have only the data you need to validate.")
                    print("Check \"validate_data_example.pydict\" file to see an example. The file can contain more than one getter to validate.")
                    print("\nYou can use additional parameters like \"_mode\": \"strict\" or \"list\". Check https://napalm.readthedocs.io/en/latest/validate/index.html to learn more.")
                    input("\nEnter any key to continue..")
                elif validate_task_choice == "3":
                    # cancel
                    validate_menu_loop = False
                else:
                    input("Wrong menu selection. Enter any key to try again..")
        elif choice == "7":
            # display help
            print("\nWelcome in Nornir automation tool!\n\nMake sure you added your devices to \"device_inventory.xlsx\" file to have them imported to the tool.")
            print("Check \"device_inventory_file_format.txt\" for help.")
            print("\nThis tool allows you to run tasks on your network devices. Tasks are separated on categorises: getters to get information from devices, configuration tasks, validate tasks to see if your devices are compliant. Any other tasks like test, delete file, clear sessions etc. are grouped in other tasks.")
            print("This tool uses NAPALM, therefore, can be easily extended to work on multiple vendors if necessary.")
            print("\nAny questions? Damo at Opticore")
            input("\nEnter any key to continue..")
        elif choice == "8":
            print("Exiting..")
            menu_loop = False
        else:
            input("Wrong menu selection. Enter any key to try again..")


def print_group_members(group_name):
    """Print group members and inform if group not exists"""
    
    group_name=group_name.lower()
    try:
        if nr.inventory.groups[group_name] != 0:
            print(f"\n  {len(nr.inventory.children_of_group(group_name))} hosts in group \"{group_name}\":\n")
            pprint(nr.inventory.children_of_group(group_name), indent=10)
            print()
        else:
            print(f"\n Group \"{group_name}\" has no hosts")
            return False
    except KeyError:
        print(f"\n Group \"{group_name}\" is not in inventory file or you provided incorrect data.")
        return False

def print_filtered_hosts(f_name, f_value):
    """Arguments:
        f_name(str): filter name
        f_value(str): filter value
    Print filtered hosts e.g. f_name = site, f_value = london
    will print all hosts that have "site" specified as "london"
    """
    f_name_exp = f_name + "__contains" # if attribute is a list of objects
    
    f_value = f_value.lower()

    if len(nr.filter(**{f_name: f_value}).inventory.hosts) != 0:
        # if filtered inventory has hosts print them and return the 
        # filtered inventory - will return 0 if an attribute (f_name) is
        # a list of objects e.g VLAN=[200,100,999]
        print(f"\n  {len(nr.filter(**{f_name: f_value}).inventory.hosts)} hosts filtered by {f_name}={f_value}:\n")
        pprint(nr.filter(**{f_name: f_value}).inventory.hosts, indent=10)
        return nr.filter(**{f_name: f_value})
    elif len(nr.filter(F(**{f_name_exp: f_value})).inventory.hosts) != 0:
        # check if f_name is a list of objects, if matches some hosts print
        # them and return filtered inventory
        print(f"\n  {len(nr.filter(F(**{f_name_exp: f_value})).inventory.hosts)} hosts filtered by {f_name_exp}={f_value}:\n")
        pprint(nr.filter(F(**{f_name_exp: f_value})).inventory.hosts, indent=10)
        return nr.filter(F(**{f_name_exp: f_value}))
    else:
        print(f"\n There are no hosts with {f_name}={f_value} or you provided wrong data.")
        return False
                
def provide_creds():
    login = input("\nProvide login: ")
    password = getpass.getpass(prompt="Provide password: ")
    print()
    nr.inventory.defaults.username = login
    nr.inventory.defaults.password = password
    # enable password used by various platforms
    nr.inventory.defaults.connection_options["napalm"].extras["optional_args"]["secret"] = password
    nr.inventory.defaults.connection_options["napalm"].extras["optional_args"]["enable_password"] = password


# create Nornir inventory from the ExcelInventory plugin (Excel file)
nr = InitNornir(
    core={"num_workers": 100},
    inventory={
        "plugin": "CustomNornirInventory.ExcelInventory",
    }
)

# create options that are used by NAPALM, 
connection_options = {}
connection_options["napalm"] = ConnectionOptions(
    extras={
        "optional_args": {
            "auto_probe": 30, # required for EOS
            "inline_transfer": True # if false SCP is used (a way to 
            # deploy config onto devices and transefer candidate, 
            # rollback, merge configs). inline transfer works only with 
            # Cisco IOS. Default value False (SCP) - suggested.
            # More info in main menu help section.     
        }
    }
)
nr.inventory.defaults.connection_options = connection_options

if __name__ == "__main__":
    menu()